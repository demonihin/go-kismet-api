package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestChannelsSuite struct {
	suite.Suite
}

func (t *TestChannelsSuite) TestGetChannelsContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	channelsReport, err := cl.GetChannelsContext(context.Background())
	t.NoError(err)
	t.NotNil(channelsReport)

	out, _ := json.MarshalIndent(channelsReport, "", "  ")
	fmt.Println(string(out))
}

func TestRunChannelsSuite(t *testing.T) {
	suite.Run(t, &TestChannelsSuite{})
}
