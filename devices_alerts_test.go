package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestDevicesAlertsSuite struct {
	suite.Suite
}

func (t *TestDevicesAlertsSuite) TestGetDevicesAlertsPresenceAbsenceFoundContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsFound)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesAlertsSuite) TestGetDevicesAlertsPresenceAbsenceLostContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsLost)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesAlertsSuite) TestGetDevicesAlertsPresenceAbsenceBothContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsBoth)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesAlertsSuite) TestAddDevicesAlertsPresenceAbsenceFoundContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.AddDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsFound, "99:88:77:66:55:44")
	t.NoError(err)
}

func (t *TestDevicesAlertsSuite) TestAddDevicesAlertsPresenceAbsenceLostContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.AddDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsLost, "99:88:77:66:55:44")
	t.NoError(err)
}

func (t *TestDevicesAlertsSuite) TestAddDevicesAlertsPresenceAbsenceBothContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.AddDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsBoth, "99:88:77:66:55:44")
	t.NoError(err)
}

func (t *TestDevicesAlertsSuite) TestRemoveDevicesAlertsPresenceAbsenceFoundContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.RemoveDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsFound, "99:88:77:66:55:44")
	t.NoError(err)
}

func (t *TestDevicesAlertsSuite) TestRemoveDevicesAlertsPresenceAbsenceLostContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.RemoveDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsLost, "99:88:77:66:55:44")
	t.NoError(err)
}

func (t *TestDevicesAlertsSuite) TestRemoveDevicesAlertsPresenceAbsenceBothContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.RemoveDevicesAlertsPresenceAbsenceContext(context.Background(), DevicesAlertsBoth, "99:88:77:66:55:44")
	t.NoError(err)
}

func TestRunDevicesAlertsSuite(t *testing.T) {
	suite.Run(t, &TestDevicesAlertsSuite{})
}
