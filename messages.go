package kismet

import (
	"context"
	"fmt"
	"net/url"
	"strconv"
)

const (
	messagesAllURL    = "/messagebus/all_messages.json"
	messagesRecentURL = "/messagebus/last-time/%s/messages.json"
)

// Message - a Kismet message.
//easyjson:json
type Message struct {
	Time   TimeUSec `json:"kismet.messagebus.message_time"`
	Flags  int64    `json:"kismet.messagebus.message_flags"`
	String string   `json:"kismet.messagebus.message_string"`
}

// MessageList - a list of Messages with a Timestamp when the list was generated
// which can be used in GetMessagesLastSeenContext.
//easyjson:json
type MessageList struct {
	Timestamp TimeUSec  `json:"kismet.messagebus.timestamp"`
	List      []Message `json:"kismet.messagebus.list"`
}

// GetMessagesAllContext - returns All messages. In fact, returns last N messages
// where N is configured in Kismet configuration and 50 by default.
// https://www.kismetwireless.net/docs/devel/webui_rest/messages/#all-messages.
func (cl *Client) GetMessagesAllContext(ctx context.Context) ([]Message, error) {
	var lastMessages = make([]Message, 0, 50) //nolint:gomnd // 50 messages is the maximum in this API endpoint by default.

	if err := cl.postRequestContext(ctx, messagesAllURL, nil, &lastMessages); err != nil {
		return nil, err
	}

	return lastMessages, nil
}

// GetMessagesAll - returns All messages. In fact, returns last N messages
// where N is configured in Kismet configuration and 50 by default.
// https://www.kismetwireless.net/docs/devel/webui_rest/messages/#all-messages.
func (cl *Client) GetMessagesAll() ([]Message, error) {
	return cl.GetMessagesAllContext(context.Background())
}

// GetMessagesLastSeenContext - should return all messages after the given timestamp
// but returns all messages for me.
// ToDo: fix. Bug.
// https://www.kismetwireless.net/docs/devel/webui_rest/messages/#recent-messages.
func (cl *Client) GetMessagesLastSeenContext(ctx context.Context, timestamp int64) (*MessageList, error) {
	var lastMessages MessageList

	if err := cl.postRequestContext(
		ctx,
		fmt.Sprintf(messagesRecentURL, url.PathEscape(strconv.FormatInt(timestamp, 10))),
		nil,
		&lastMessages); err != nil {
		return nil, err
	}

	return &lastMessages, nil
}

// GetMessagesLastSeen - should return all messages after the given timestamp
// but returns all messages for me.
// ToDo: fix. Bug.
// https://www.kismetwireless.net/docs/devel/webui_rest/messages/#recent-messages.
func (cl *Client) GetMessagesLastSeen(timestamp int64) (*MessageList, error) {
	return cl.GetMessagesLastSeenContext(context.Background(), timestamp)
}
