package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestMessagesSuite struct {
	suite.Suite
}

func (t *TestMessagesSuite) TestGetMessagesAllContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	messages, err := cl.GetMessagesAllContext(context.Background())
	t.NoError(err)
	t.NotNil(messages)

	out, _ := json.MarshalIndent(messages, "", "  ")
	fmt.Println(string(out))
}

func (t *TestMessagesSuite) TestGetMessagesLastSeenContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	messages, err := cl.GetMessagesLastSeenContext(context.Background(), -10)
	t.NoError(err)
	t.NotNil(messages)

	out, _ := json.MarshalIndent(messages, "", "  ")
	fmt.Println(string(out))
}

func TestRunMessagesSuite(t *testing.T) {
	suite.Run(t, &TestMessagesSuite{})
}
