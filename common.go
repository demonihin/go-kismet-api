package kismet

// CommonRrd - contains Round Robin statistics for interval ranges.
//easyjson:json
type CommonRrd struct {
	// BlankVal - blank value.
	BlankVal int64 `json:"kismet.common.rrd.blank_val"`
	// DayVec - past day values per hour.
	DayVec []float64 `json:"kismet.common.rrd.day_vec"`
	// HourVec - past hour values per minute.
	HourVec []float64 `json:"kismet.common.rrd.hour_vec"`
	// MinuteVec - past minute values per second.
	MinuteVec []float64 `json:"kismet.common.rrd.minute_vec"`
	// SerialTime - timestamp of serialization.
	SerialTime TimeUSec `json:"kismet.common.rrd.serial_time"`
	// LastTime - last time updated.
	LastTime TimeUSec `json:"kismet.common.rrd.last_time"`
}

// CommonSeenBy - a reference to a Kismet which detected some packet/SSID etc.
//easyjson:json
type CommonSeenBy struct {
	// UUID - UUID of source.
	UUID string `json:"kismet.common.seenby.uuid"`
	// NumPackets - number of packets seen by this device.
	NumPackets uint64 `json:"kismet.common.seenby.num_packets"`
	// LastTime - last time seen time_t.
	LastTime uint64 `json:"kismet.common.seenby.last_time"`
	// FirstTime - first time seen time_t.
	FirstTime int64 `json:"kismet.common.seenby.first_time"`
}

// FieldSimplification - a list of fields to retrieve from supporting methods.
// Only fields in the FieldSimplification will be loaded from Kismet API.
type FieldSimplification []string

/*
RegexMatch - the Regex matcher in Kismet is a value in HTTP POST which is:
[ 'dot11.device/dot11.device.advertised_ssid_map/dot11.advertisedssid.ssid', '^SomePrefix.*' ].
so, it is an array of 2 element arrays.
*/
type RegexMatch [2]string

/*
RegexFilters - list of RegexMatch to use them as "OR".
regex = [
    [ 'dot11.device/dot11.device.advertised_ssid_map/dot11.advertisedssid.ssid', '^SomePrefix.*' ],
    [ 'dot11.device/dot11.device.advertised_ssid_map/dot11.advertisedssid.ssid', '^Linksys$' ]
]
*/
type RegexFilters []RegexMatch

// QueryOptions - options for API methods.
//easyjson:json
type QueryOptions struct {
	Fields      FieldSimplification `json:"fields,omitempty"`
	RegexFilter RegexFilters        `json:"regex,omitempty"`
	Devices     []string            `json:"devices,omitempty"`
}

// commandDictionary Kismet command methods' settings.
type commandDictionary map[string]interface{}

// CommonSignal - common signal information.
//easyjson:json
type CommonSignal struct {
	// Carrierset - bitset of observed carrier types.
	Carrierset uint64 `json:"kismet.common.signal.carrierset"`
	// Encodingset - bitset of observed encodings.
	Encodingset uint64 `json:"kismet.common.signal.encodingset"`
	// Maxseenrate - maximum observed data rate (phy dependent).
	Maxseenrate float64 `json:"kismet.common.signal.maxseenrate"`
	// MaxNoise - maximum noise.
	MaxNoise int32 `json:"kismet.common.signal.max_noise"`
	// MaxSignal - maximum signal.
	MaxSignal int32 `json:"kismet.common.signal.max_signal"`
	// MinNoise - minimum noise.
	MinNoise int32 `json:"kismet.common.signal.min_noise"`
	// MinSignal - minimum signal.
	MinSignal int32 `json:"kismet.common.signal.min_signal"`
	// LastNoise - most recent noise.
	LastNoise int32 `json:"kismet.common.signal.last_noise"`
	// LastSignal - most recent signal.
	LastSignal int32 `json:"kismet.common.signal.last_signal"`
	// Type - signal type.
	Type      CommonSignalType `json:"kismet.common.signal.type"`
	SignalRrd *CommonRrd       `json:"kismet.common.signal.signal_rrd,omitempty"`
}
