package kismet

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestTimeUSecSuite struct {
	suite.Suite
}

func (t *TestTimeUSecSuite) TestParseSuccess() {
	var tests = []struct {
		raw    []byte
		parsed TimeUSec
	}{
		{
			raw: []byte("123123.456"),
			parsed: TimeUSec{
				Seconds:      123123,
				Microseconds: 456,
			},
		},
		{
			raw: []byte("-123123.456"),
			parsed: TimeUSec{
				Seconds:      -123123,
				Microseconds: 456,
			},
		},
		{
			raw: []byte("123123"),
			parsed: TimeUSec{
				Seconds:      123123,
				Microseconds: 0,
			},
		},
		{
			raw: []byte("-123123"),
			parsed: TimeUSec{
				Seconds:      -123123,
				Microseconds: 0,
			},
		},
	}

	for _, test := range tests {
		var parsed TimeUSec

		err := json.Unmarshal(test.raw, &parsed)
		t.NoError(err)

		t.Equal(test.parsed, parsed)
	}
}

func (t *TestTimeUSecSuite) TestParseFailure() {
	var tests = []struct {
		raw []byte
	}{
		{
			raw: []byte("--123123.456"),
		},
		{
			raw: []byte("-123123.45.6"),
		},
		{
			raw: []byte("123.1.23"),
		},
		{
			raw: []byte("-"),
		},
		{
			raw: []byte(""),
		},
	}

	for _, test := range tests {
		var parsed TimeUSec

		err := json.Unmarshal(test.raw, &parsed)
		t.Error(err)
	}
}

func TestRunTestTimeUSecSuite(t *testing.T) {
	suite.Run(t, &TestTimeUSecSuite{})
}
