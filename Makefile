help:
	@echo "generate - uses easyjson to generate struct JSON marshalers and unmarshalers"
	@echo "install-easyjson - uses go commands to download and install easyjson binary"

generate:
	easyjson -pkg

install-easyjson:
	# for Go >= 1.17
	go get -v github.com/mailru/easyjson
	go install -v github.com/mailru/easyjson/...