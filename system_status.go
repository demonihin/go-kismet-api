package kismet

import (
	"context"
)

const (
	systemStatusURL      = "/system/status.json"
	systemTimestampURL   = "/system/timestamp.json"
	systemPacketStatsURL = "/packetchain/packet_stats.json"
)

// SystemStatus - status of Kismet in general.
//easyjson:json
type SystemStatus struct {
	ServerUUID               string             `json:"kismet.server.uuid"`
	SystemNumHTTPConnections int                `json:"kismet.system.num_http_connections"`
	SystemNumComponents      int                `json:"kismet.system.num_components"`
	SystemNumFields          int                `json:"kismet.system.num_fields"`
	SystemSensorsTemp        map[string]float64 `json:"kismet.system.sensors.temp"`
	SystemSensorsFan         map[string]float64 `json:"kismet.system.sensors.fan"`
	SystemDevicesRrd         CommonRrd          `json:"kismet.system.devices.rrd"`
	SystemMemoryRrd          CommonRrd          `json:"kismet.system.memory.rrd"`
	SystemServerLocation     string             `json:"kismet.system.server_location"`
	SystemServerDescription  string             `json:"kismet.system.server_description"`
	SystemServerName         string             `json:"kismet.system.server_name"`
	SystemBatteryPercentage  int                `json:"kismet.system.battery.percentage"`
	SystemBatteryCharging    string             `json:"kismet.system.battery.charging"`
	SystemBatteryAC          int                `json:"kismet.system.battery.ac"`
	SystemBatteryRemaining   int                `json:"kismet.system.battery.remaining"`
	SystemTimestampSEC       int                `json:"kismet.system.timestamp.sec"`
	SystemTimestampUsec      int                `json:"kismet.system.timestamp.usec"`
	SystemTimestampStartSEC  int                `json:"kismet.system.timestamp.start_sec"`
	SystemMemoryRSS          int                `json:"kismet.system.memory.rss"`
	SystemDevicesCount       int                `json:"kismet.system.devices.count"`
	SystemUser               string             `json:"kismet.system.user"`
	SystemVersion            string             `json:"kismet.system.version"`
	SystemGit                string             `json:"kismet.system.git"`
	SystemBuildTime          string             `json:"kismet.system.build_time"`
}

// SystemPacketStats - packets statistics.
//easyjson:json
type SystemPacketStats struct {
	ProcessedPacketsRrd CommonRrd `json:"kismet.packetchain.processed_packets_rrd"`
	DroppedPacketsRrd   CommonRrd `json:"kismet.packetchain.dropped_packets_rrd"`
	QueuedPacketsRrd    CommonRrd `json:"kismet.packetchain.queued_packets_rrd"`
	DupePacketsRrd      CommonRrd `json:"kismet.packetchain.dupe_packets_rrd"`
	ErrorPacketsRrd     CommonRrd `json:"kismet.packetchain.error_packets_rrd"`
	PacketsRrd          CommonRrd `json:"kismet.packetchain.packets_rrd"`
	PeakPacketsRrd      CommonRrd `json:"kismet.packetchain.peak_packets_rrd"`
}

// SystemTimestamp - current Kismet server timestamp.
// I can not use TimeUSec here because the TimeUSec uses custom JSON serialization.
//easyjson:json
type SystemTimestamp struct {
	SystemTimestampUsec int `json:"kismet.system.timestamp.usec"`
	SystemTimestampSEC  int `json:"kismet.system.timestamp.sec"`
}

// GetSystemStatusContext - returns current status.
// https://www.kismetwireless.net/docs/devel/webui_rest/system_status/#system-status.
func (cl *Client) GetSystemStatusContext(ctx context.Context, fields FieldSimplification) (*SystemStatus, error) {
	var status SystemStatus

	if err := cl.postRequestContext(ctx, systemStatusURL, &QueryOptions{Fields: fields}, &status); err != nil {
		return nil, err
	}

	return &status, nil
}

// GetSystemStatus - returns current status.
// https://www.kismetwireless.net/docs/devel/webui_rest/system_status/#system-status.
func (cl *Client) GetSystemStatus(fields FieldSimplification) (*SystemStatus, error) {
	return cl.GetSystemStatusContext(context.Background(), fields)
}

// GetSystemTimestampContext - returns current Kismet server time.
// https://www.kismetwireless.net/docs/devel/webui_rest/system_status/#timestamp.
func (cl *Client) GetSystemTimestampContext(ctx context.Context) (*SystemTimestamp, error) {
	var ts SystemTimestamp

	if err := cl.postRequestContext(ctx, systemTimestampURL, nil, &ts); err != nil {
		return nil, err
	}

	return &ts, nil
}

// GetSystemTimestamp - returns current Kismet server time.
// https://www.kismetwireless.net/docs/devel/webui_rest/system_status/#timestamp.
func (cl *Client) GetSystemTimestamp() (*SystemTimestamp, error) {
	return cl.GetSystemTimestampContext(context.Background())
}

// GetSystemPacketStatsContext - returns current packets statistics.
// https://www.kismetwireless.net/docs/devel/webui_rest/system_status/#timestamp.
func (cl *Client) GetSystemPacketStatsContext(ctx context.Context, fields FieldSimplification) (*SystemPacketStats, error) {
	var ps SystemPacketStats

	if err := cl.postRequestContext(ctx, systemPacketStatsURL, &QueryOptions{Fields: fields}, &ps); err != nil {
		return nil, err
	}

	return &ps, nil
}

// GetSystemPacketStats - returns current packets statistics.
// https://www.kismetwireless.net/docs/devel/webui_rest/system_status/#timestamp.
func (cl *Client) GetSystemPacketStats(fields FieldSimplification) (*SystemPacketStats, error) {
	return cl.GetSystemPacketStatsContext(context.Background(), fields)
}
