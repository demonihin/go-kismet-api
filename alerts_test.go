package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestAlertsSuite struct {
	suite.Suite
}

func (t *TestAlertsSuite) TestGetAlertsDefinitionsContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	alertDefs, err := cl.GetAlertsDefinitionsContext(context.Background())
	t.NoError(err)
	t.NotNil(alertDefs)

	out, _ := json.MarshalIndent(alertDefs, "", "  ")
	fmt.Println(string(out))
}

func (t *TestAlertsSuite) TestGetAlertsAllContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	alerts, err := cl.GetAlertsAllContext(context.Background())
	t.NoError(err)
	t.NotNil(alerts)

	out, _ := json.MarshalIndent(alerts, "", "  ")
	fmt.Println(string(out))
}

func (t *TestAlertsSuite) TestGetAlertsLastSeenContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	alerts, err := cl.GetAlertsLastSeenContext(context.Background(), TimeUSec{Seconds: 1640623058})
	t.NoError(err)
	t.NotNil(alerts)

	out, _ := json.MarshalIndent(alerts, "", "  ")
	fmt.Println(string(out))
}

func (t *TestAlertsSuite) TestGetAlertDetailsContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	alerts, err := cl.GetAlertDetailsContext(context.Background(), "2530416183", nil)
	t.NoError(err)
	t.NotNil(alerts)

	out, _ := json.MarshalIndent(alerts, "", "  ")
	fmt.Println(string(out))
}

func TestRunAlertsSuite(t *testing.T) {
	suite.Run(t, &TestAlertsSuite{})
}
