package kismet

import (
	"context"
	"fmt"
	"net/url"
)

const (
	devicesAlertsGetPresenceAbsenceURL    = "/devices/alerts/mac/%s/macs.json"
	devicesAlertsAddPresenceAbsenceURL    = "/devices/alerts/mac/%s/add.cmd"
	devicesAlertsRemovePresenceAbsenceURL = "/devices/alerts/mac/%s/remove.cmd"
)

// DevicesAlertsPresenceAbsenceType - type of Device alerts.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#alerts---device-presence--absence---viewing.
type DevicesAlertsPresenceAbsenceType string

const (
	DevicesAlertsFound DevicesAlertsPresenceAbsenceType = "found"
	DevicesAlertsLost  DevicesAlertsPresenceAbsenceType = "lost"
	DevicesAlertsBoth  DevicesAlertsPresenceAbsenceType = "both"
)

// GetDevicesAlertsPresenceAbsenceContext - returns Device presence/absence Alerts.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#alerts---device-presence--absence---viewing.
func (cl *Client) GetDevicesAlertsPresenceAbsenceContext(ctx context.Context, alertType DevicesAlertsPresenceAbsenceType) ([]string, error) { //nolint:lll
	var macs []string

	if err := cl.getRequestContext(
		ctx,
		fmt.Sprintf(devicesAlertsGetPresenceAbsenceURL, url.PathEscape(string(alertType))),
		nil,
		&macs,
	); err != nil {
		return nil, err
	}

	return macs, nil
}

// GetDevicesAlertsPresenceAbsence - returns Device presence/absence Alerts.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#alerts---device-presence--absence---viewing.
func (cl *Client) GetDevicesAlertsPresenceAbsence(alertType DevicesAlertsPresenceAbsenceType) ([]string, error) {
	return cl.GetDevicesAlertsPresenceAbsenceContext(context.Background(), alertType)
}

// AddDevicesAlertsPresenceAbsenceContext - Add Device Alerts to Kismet.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#alerts---device-presence--absence---changing.
func (cl *Client) AddDevicesAlertsPresenceAbsenceContext(ctx context.Context, alertType DevicesAlertsPresenceAbsenceType, macs ...string) error { //nolint:lll
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(devicesAlertsAddPresenceAbsenceURL, url.PathEscape(string(alertType))),
		commandDictionary{"macs": macs},
	)
}

// AddDevicesAlertsPresenceAbsence - Add Device Alerts to Kismet.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#alerts---device-presence--absence---changing.
func (cl *Client) AddDevicesAlertsPresenceAbsence(alertType DevicesAlertsPresenceAbsenceType, macs ...string) error {
	return cl.AddDevicesAlertsPresenceAbsenceContext(context.Background(), alertType, macs...)
}

// RemoveDevicesAlertsPresenceAbsenceContext - Remove Device Alerts from Kismet.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#realtime-device-monitoring.
func (cl *Client) RemoveDevicesAlertsPresenceAbsenceContext(ctx context.Context, alertType DevicesAlertsPresenceAbsenceType, macs ...string) error { //nolint:lll
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(devicesAlertsRemovePresenceAbsenceURL, url.PathEscape(string(alertType))),
		commandDictionary{"macs": macs},
	)
}

// RemoveDevicesAlertsPresenceAbsence - Remove Device Alerts from Kismet.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#realtime-device-monitoring.
func (cl *Client) RemoveDevicesAlertsPresenceAbsence(alertType DevicesAlertsPresenceAbsenceType, macs ...string) error {
	return cl.RemoveDevicesAlertsPresenceAbsenceContext(context.Background(), alertType, macs...)
}
