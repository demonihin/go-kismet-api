package kismet

import "context"

const (
	physAllURL = "/phy/all_phys.json"
)

// Phy - one supported Phy.
//easyjson:json
type Phy struct {
	PhyPacketCount int64  `json:"kismet.phy.packet_count"`
	PhyDeviceCount int64  `json:"kismet.phy.device_count"`
	PhyPhyID       int64  `json:"kismet.phy.phy_id"`
	PhyPhyName     string `json:"kismet.phy.phy_name"`
}

// GetPhysAllContext - returns all supported Phy types.
func (cl *Client) GetPhysAllContext(ctx context.Context, fields FieldSimplification) ([]Phy, error) {
	var phys []Phy

	if err := cl.postRequestContext(ctx, physAllURL, &QueryOptions{Fields: fields}, &phys); err != nil {
		return nil, err
	}

	return phys, nil
}

// GetPhysAll - returns all supported Phy types.
func (cl *Client) GetPhysAll(fields FieldSimplification) ([]Phy, error) {
	return cl.GetPhysAllContext(context.Background(), fields)
}
