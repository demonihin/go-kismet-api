# Kismet Go API Client

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/demonihin/go-kismet-api.svg)](https://pkg.go.dev/gitlab.com/demonihin/go-kismet-api)

This library implements partially API client for [Kismet](https://www.kismetwireless.net/) server.

At the moment NOT implemented:

- All Websocket based APIs
- GPS
- All packet capture APIs.

APIs use a simple `json` endpoint and not `ekjson` or `itjson` so it is possible
that the client's performance might be not the best ([supported Kismet API types](https://www.kismetwireless.net/docs/devel/webui_rest/serialization/)).

An issue with [messages API exists](https://www.kismetwireless.net/docs/devel/webui_rest/messages/#recent-messages) in the library: Kismet returns all and not the latest messages. I probably did something wrong.