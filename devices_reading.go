package kismet

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
)

const (
	devicesLastSeenURL       = "/devices/last-time/%d/devices.json"
	devicesLastSeenStreamURL = "/devices/last-time/%d/devices.itjson"

	devicesSeenBySourceUUIDURL         = "/devices/views/seenby-uuid/%s/devices.json"
	devicesSeenBySourceUUIDLastSeenURL = "/devices/views/seenby-uuid/%s/last-time/%d/devices.json"

	deviceByKeyURL              = "/devices/by-key/%s/device.json"
	devicesMultipleByKeyURL     = "/devices/multikey/devices.json"
	devicesMultipleByKeyDictURL = "/devices/multikey/as-object/devices.json"

	devicesByMacURL         = "/devices/by-mac/%s/devices.json"
	devicesMultipleByMacURL = "/devices/multimac/devices.json"

	devicesViewDevicesURL       = "/devices/views/%s/devices.json"
	devicesViewDevicesStreamURL = "/devices/views/%s/devices.itjson"

	devicesViewLastSeenURL       = "/devices/views/%s/last-time/%d/devices.json"
	devicesViewLastSeenStreamURL = "/devices/views/%s/last-time/%d/devices.itjson"

	devicesByPhyTypeUUIDURL     = "/devices/views/phy/%s/devices.json"
	devicesByPhyTypeLastSeenURL = "/devices/views/phy/%s/last-time/%d/devices.json"

	devicesDot11DeviceClientsURL        = "/phy/phy80211/clients-of/%s/clients.json"
	devicesDot11AccessPointsURL         = "/devices/views/phydot11_accesspoints/devices.json"
	devicesDot11AccessPointsLastSeenURL = "/devices/views/phydot11_accesspoints/last-time/%d/devices.json"
	devicesDot11RelatedDevicesURL       = "/phy/phy80211/related-to/%s/devices.json"
)

/*
 Device Base data structures.
*/

// DeviceBaseManuf - device manufacturer.
type DeviceBaseManuf string

const (
	BeijingXiaomiElectronicsLtd DeviceBaseManuf = "Beijing Xiaomi Electronics Ltd"
	BoseCorporation             DeviceBaseManuf = "Bose Corporation"
	LogitechInc                 DeviceBaseManuf = "Logitech Inc."
	RaysonTechnologyLtd         DeviceBaseManuf = "Rayson Technology Ltd"
	SamsungElectronicsLtd       DeviceBaseManuf = "Samsung Electronics Ltd"
	Unknown                     DeviceBaseManuf = "Unknown"
)

// DeviceBaseChannel base channel name.
type DeviceBaseChannel string

const (
	Fhss DeviceBaseChannel = "FHSS"
)

// DeviceBaseType - device type.
type DeviceBaseType string

const (
	BrEdr              DeviceBaseType = "BR/EDR"
	BtLE               DeviceBaseType = "BTLE"
	WiFiAP             DeviceBaseType = "Wi-Fi AP"
	WiFiBridged        DeviceBaseType = "Wi-Fi Bridged"
	WiFiDevice         DeviceBaseType = "Wi-Fi Device"
	WiFiDeviceInferred DeviceBaseType = "Wi-Fi Device (Inferred)"
)

// CommonSignalType -
type CommonSignalType string

const (
	Dbm  CommonSignalType = "dbm"
	None CommonSignalType = "none"
)

// DeviceBasePhyname - a Device's PHY name.
type DeviceBasePhyname string

const (
	Bluetooth DeviceBasePhyname = "Bluetooth"
	Ieee80211 DeviceBasePhyname = "IEEE802.11"
)

// DeviceBaseRelatedDevices - devices related to a device which has this structure.
//easyjson:json
type DeviceBaseRelatedDevices struct {
	Dot11BsstsSimilar []string `json:"dot11_bssts_similar,omitempty"`
}

// Device - main structure with a detected Device information.
//easyjson:json
type Device struct {
	// Type - printable device type.
	Type DeviceBaseType `json:"kismet.device.base.type"`
	// BluetoothDevice - Bluetooth device.
	BluetoothDevice *BluetoothDevice `json:"bluetooth.device,omitempty"`
	// Dot11Device - IEEE802.11 device.
	Dot11Device *Dot11Device `json:"dot11.device,omitempty"`
	// Manuf - manufacturer name.
	Manuf string `json:"kismet.device.base.manuf"`
	// ServerUUID - unique server UUID.
	ServerUUID ServerUUID `json:"kismet.server.uuid"`
	// Seenby - sources that have seen this device.
	Seenby []CommonSeenBy `json:"kismet.device.base.seenby"`
	// NumAlerts - number of alerts on this device.
	NumAlerts uint32 `json:"kismet.device.base.num_alerts"`
	// ModTime - timestamp of last seen time (local clock).
	ModTime uint64 `json:"kismet.device.base.mod_time"`
	// LastTime - last time seen time_t.
	LastTime uint64 `json:"kismet.device.base.last_time"`
	// FirstTime - first time seen time_t.
	FirstTime uint64 `json:"kismet.device.base.first_time"`
	// Frequency - frequency.
	Frequency float64 `json:"kismet.device.base.frequency"`
	// BasicCryptSet - bitset of basic encryption.
	BasicCryptSet uint64 `json:"kismet.device.base.basic_crypt_set"`
	// Channel - channel (phy specific).
	Channel string `json:"kismet.device.base.channel"`
	// Crypt - printable encryption type.
	Crypt DeviceBaseCryptEnum `json:"kismet.device.base.crypt"`
	// FreqKhzMap - packets seen per frequency (khz).
	FreqKhzMap map[float64]float64 `json:"kismet.device.base.freq_khz_map"`
	// BasicTypeSet - bitset of basic type.
	BasicTypeSet uint64 `json:"kismet.device.base.basic_type_set"`
	// Signal - map[field, x], signal data.
	Signal CommonSignal `json:"kismet.device.base.signal"`
	// Commonname - common name alias of custom or device names.
	Commonname string `json:"kismet.device.base.commonname"`
	// Name - printable device name.
	Name string `json:"kismet.device.base.name"`
	// Phyname - phy name.
	Phyname DeviceBasePhyname `json:"kismet.device.base.phyname"`
	// RelatedDevices - Related devices, organized by relationship.
	RelatedDevices DeviceBaseRelatedDevices `json:"kismet.device.base.related_devices"`
	// Macaddr - mac address.
	Macaddr string `json:"kismet.device.base.macaddr"`
	// Key - unique device key across phy and server.
	Key string `json:"kismet.device.base.key"`
	// PacketsTotal - total packets seen of all types.
	PacketsTotal uint64 `json:"kismet.device.base.packets.total"`
	PacketsRx    uint64 `json:"kismet.device.base.packets.rx"`
	PacketsTx    uint64 `json:"kismet.device.base.packets.tx"`
	// PacketsLlc - observed protocol control packets.
	PacketsLlc uint64 `json:"kismet.device.base.packets.llc"`
	// PacketsError - corrupt/error packets.
	PacketsError uint64 `json:"kismet.device.base.packets.error"`
	// PacketsData - data packets.
	PacketsData uint64 `json:"kismet.device.base.packets.data"`
	// PacketsCrypt - data packets using encryption.
	PacketsCrypt uint64 `json:"kismet.device.base.packets.crypt"`
	// PacketsFiltered - packets dropped by filter.
	PacketsFiltered uint64 `json:"kismet.device.base.packets.filtered"`
	// Datasize - transmitted data in bytes.
	Datasize uint64 `json:"kismet.device.base.datasize"`
	// PacketsRrd - map[field, x], packet rate rrd.
	PacketsRrd  *CommonRrd `json:"kismet.device.base.packets.rrd,omitempty"`
	DatasizeRrd *CommonRrd `json:"kismet.device.base.datasize.rrd,omitempty"`
}

// ServerUUID - Kismet server UUID.
type ServerUUID string

func (cl *Client) getDevicesContext(ctx context.Context, reqURL string, options *QueryOptions) ([]Device, error) {
	var devices []Device

	if err := cl.postRequestContext(ctx,
		reqURL,
		options,
		&devices); err != nil {
		return nil, err
	}

	return devices, nil
}

func (cl *Client) streamDevicesContext(ctx context.Context,
	reqURL string, options *QueryOptions) (DevicesStreamer, error) {
	return newDevicesStream(ctx, cl, http.MethodPost, reqURL, options)
}

func (cl *Client) getDeviceContext(ctx context.Context, reqURL string, options *QueryOptions) (*Device, error) {
	var device Device

	if err := cl.postRequestContext(ctx,
		reqURL,
		options,
		&device); err != nil {
		return nil, err
	}

	return &device, nil
}

func (cl *Client) getDevicesMapContext(ctx context.Context, reqURL string, options *QueryOptions) (map[string]Device, error) {
	var devices map[string]Device

	if err := cl.postRequestContext(ctx,
		reqURL,
		options,
		&devices); err != nil {
		return nil, err
	}

	return devices, nil
}

// GetDevicesViewContext - returns Devices optionally filtered by Regexes and with Fields simplification.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#device-summary-and-windowed-device-view
// https://www.kismetwireless.net/docs/devel/webui_rest/device_views/#device-views.
func (cl *Client) GetDevicesViewContext(ctx context.Context,
	viewID string, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(devicesViewDevicesURL, url.PathEscape(viewID)),
		&QueryOptions{
			Fields:      fields,
			RegexFilter: regexes,
		},
	)
}

// GetDevicesView - returns Devices optionally filtered by Regexes and with Fields simplification.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#device-summary-and-windowed-device-view
// https://www.kismetwireless.net/docs/devel/webui_rest/device_views/#device-views.
func (cl *Client) GetDevicesView(viewID string, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDevicesViewContext(context.Background(), viewID, fields, regexes)
}

// StreamDevicesViewContext - streams Devices optionally filtered by Regexes and with Fields simplification.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#device-summary-and-windowed-device-view
// https://www.kismetwireless.net/docs/devel/webui_rest/device_views/#device-views.
func (cl *Client) StreamDevicesViewContext(ctx context.Context,
	viewID string, fields FieldSimplification, regexes RegexFilters) (DevicesStreamer, error) {
	return cl.streamDevicesContext(
		ctx,
		fmt.Sprintf(devicesViewDevicesStreamURL, url.PathEscape(viewID)),
		&QueryOptions{
			Fields:      fields,
			RegexFilter: regexes,
		},
	)
}

// StreamDevicesView - streams Devices optionally filtered by Regexes and with Fields simplification.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#device-summary-and-windowed-device-view
// https://www.kismetwireless.net/docs/devel/webui_rest/device_views/#device-views.
func (cl *Client) StreamDevicesView(
	viewID string, fields FieldSimplification, regexes RegexFilters) (DevicesStreamer, error) {
	return cl.StreamDevicesViewContext(context.Background(), viewID, fields, regexes)
}

// GetDevicesViewLastSeenContext - returns Devices seen from a given Timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#activity--timestamp.
func (cl *Client) GetDevicesViewLastSeenContext(ctx context.Context,
	viewID string, timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(devicesViewLastSeenURL, url.PathEscape(viewID), timestamp),
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDevicesViewLastSeen - returns Devices seen from a given Timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#activity--timestamp.
func (cl *Client) GetDevicesViewLastSeen(viewID string, timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDevicesViewLastSeenContext(context.Background(), viewID, timestamp, fields, regexes)
}

// StreamDevicesViewLastSeenContext - streams Devices seen from a given Timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#activity--timestamp.
func (cl *Client) StreamDevicesViewLastSeenContext(ctx context.Context,
	viewID string, timestamp int, fields FieldSimplification, regexes RegexFilters) (DevicesStreamer, error) {
	return cl.streamDevicesContext(
		ctx,
		fmt.Sprintf(devicesViewLastSeenStreamURL, url.PathEscape(viewID), timestamp),
		&QueryOptions{
			Fields:      fields,
			RegexFilter: regexes,
		},
	)
}

// StreamDevicesViewLastSeen - streams Devices seen from a given Timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#activity--timestamp.
func (cl *Client) StreamDevicesViewLastSeen(viewID string, timestamp int,
	fields FieldSimplification, regexes RegexFilters) (DevicesStreamer, error) {
	return cl.StreamDevicesViewLastSeenContext(context.Background(), viewID, timestamp, fields, regexes)
}

// GetDevicesLastSeenContext - returns Devices seen from a given Timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#activity--timestamp.
func (cl *Client) GetDevicesLastSeenContext(ctx context.Context,
	timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(devicesLastSeenURL, timestamp),
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDevicesLastSeen - returns Devices seen from a given Timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#activity--timestamp.
func (cl *Client) GetDevicesLastSeen(timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDevicesLastSeenContext(context.Background(), timestamp, fields, regexes)
}

// StreamDevicesLastSeenContext - streams Devices seen from a given Timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#activity--timestamp.
func (cl *Client) StreamDevicesLastSeenContext(ctx context.Context,
	timestamp int, fields FieldSimplification, regexes RegexFilters) (DevicesStreamer, error) {
	return cl.streamDevicesContext(
		ctx,
		fmt.Sprintf(devicesLastSeenStreamURL, timestamp),
		&QueryOptions{
			Fields:      fields,
			RegexFilter: regexes,
		},
	)
}

// StreamDevicesLastSeen - streams Devices seen from a given Timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#activity--timestamp.
func (cl *Client) StreamDevicesLastSeen(
	timestamp int, fields FieldSimplification, regexes RegexFilters) (DevicesStreamer, error) {
	return cl.StreamDevicesLastSeenContext(context.Background(), timestamp, fields, regexes)
}

// GetDeviceByKeyContext - returns a Device using its unique kismet.device.base.key.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#device-by-key.
func (cl *Client) GetDeviceByKeyContext(ctx context.Context, kismetDeviceBaseKey string, fields FieldSimplification) (*Device, error) {
	return cl.getDeviceContext(
		ctx,
		fmt.Sprintf(deviceByKeyURL, url.PathEscape(kismetDeviceBaseKey)),
		&QueryOptions{Fields: fields},
	)
}

// GetDeviceByKey - returns a Device using its unique kismet.device.base.key.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#device-by-key.
func (cl *Client) GetDeviceByKey(kismetDeviceBaseKey string, fields FieldSimplification) (*Device, error) {
	return cl.GetDeviceByKeyContext(context.Background(), kismetDeviceBaseKey, fields)
}

// GetDevicesMultipleByKeysContext - returns a list of Devices based on their Keys (kismet.device.base.key).
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#multiple-devices-by-key.
func (cl *Client) GetDevicesMultipleByKeysContext(ctx context.Context,
	kismetDeviceBaseKeys []string, fields FieldSimplification) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		devicesMultipleByKeyURL,
		&QueryOptions{Fields: fields, Devices: kismetDeviceBaseKeys},
	)
}

// GetDevicesMultipleByKeys - returns a list of Devices based on their Keys (kismet.device.base.key).
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#multiple-devices-by-key.
func (cl *Client) GetDevicesMultipleByKeys(ctx context.Context,
	kismetDeviceBaseKeys []string, fields FieldSimplification) ([]Device, error) {
	return cl.GetDevicesMultipleByKeysContext(context.Background(), kismetDeviceBaseKeys, fields)
}

// GetDevicesMultipleByKeysContext - returns a list of Devices based on their Keys (kismet.device.base.key)
// with "key" as a Key of the returned map.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#multiple-devices-by-key.
func (cl *Client) GetDevicesMultipleByKeysDictContext(ctx context.Context,
	kismetDeviceBaseKeys []string, fields FieldSimplification) (map[string]Device, error) {
	return cl.getDevicesMapContext(
		ctx,
		devicesMultipleByKeyDictURL,
		&QueryOptions{Fields: fields, Devices: kismetDeviceBaseKeys},
	)
}

// GetDevicesMultipleByKeys - returns a list of Devices based on their Keys (kismet.device.base.key)
// with "key" as a Key of the returned map.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#multiple-devices-by-key.
func (cl *Client) GetDevicesMultipleByKeysDict(kismetDeviceBaseKeys []string, fields FieldSimplification) (map[string]Device, error) {
	return cl.GetDevicesMultipleByKeysDictContext(context.Background(), kismetDeviceBaseKeys, fields)
}

// GetDevicesByMacContext - return a Device based on its unique MAC address.
// For some device types it is possible that multiple devices are returned because multiple
// devices could have the same MAC.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#device-by-mac.
func (cl *Client) GetDevicesByMacContext(ctx context.Context, macAddress string, fields FieldSimplification) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(devicesByMacURL, url.PathEscape(macAddress)),
		&QueryOptions{Fields: fields},
	)
}

// GetDevicesByMac - return a Device based on its unique MAC address.
// For some device types it is possible that multiple devices are returned because multiple
// devices could have the same MAC.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#device-by-mac.
func (cl *Client) GetDevicesByMac(macAddress string, fields FieldSimplification) ([]Device, error) {
	return cl.GetDevicesByMacContext(context.Background(), macAddress, fields)
}

// GetDevicesMultipleByMacContext - returns multiple devices based on their MAC addresses.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#multiple-devices-by-mac.
func (cl *Client) GetDevicesMultipleByMacContext(ctx context.Context, macAddresses []string, fields FieldSimplification) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		devicesMultipleByMacURL,
		&QueryOptions{Fields: fields, Devices: macAddresses},
	)
}

// GetDevicesSeenBySourceUUIDContext - returns all devices seen by a given Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#devices-by-capture-source.
func (cl *Client) GetDevicesSeenBySourceUUIDContext(ctx context.Context,
	dataSourceUUID string, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(
			devicesSeenBySourceUUIDURL,
			url.PathEscape(dataSourceUUID),
		),
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDevicesSeenBySourceUUID - returns all devices seen by a given Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#devices-by-capture-source.
func (cl *Client) GetDevicesSeenBySourceUUID(dataSourceUUID string, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDevicesSeenBySourceUUIDContext(context.Background(), dataSourceUUID, fields, regexes)
}

// GetDevicesSeenBySourceLastSeenContext - returns "last-seen" devices seen by a given Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#devices-by-capture-source.
func (cl *Client) GetDevicesSeenBySourceLastSeenContext(ctx context.Context,
	dataSourceUUID string, timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(
			devicesSeenBySourceUUIDLastSeenURL,
			url.PathEscape(dataSourceUUID),
			timestamp,
		),
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDevicesSeenBySourceUUIDLastSeen - returns "last-seen" devices seen by a given Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#devices-by-capture-source.
func (cl *Client) GetDevicesSeenBySourceLastSeen(dataSourceUUID string,
	timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDevicesSeenBySourceLastSeenContext(context.Background(), dataSourceUUID, timestamp, fields, regexes)
}

// GetDevicesByPhyTypeContext - returns all Devices of a given PHY type.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#phy-handlers.
func (cl *Client) GetDevicesByPhyTypeContext(ctx context.Context,
	phyName string, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(
			devicesByPhyTypeUUIDURL,
			url.PathEscape(phyName),
		),
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDevicesByPhyType - returns all Devices of a given PHY type.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#phy-handlers.
func (cl *Client) GetDevicesByPhyType(phyName string, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDevicesByPhyTypeContext(context.Background(), phyName, fields, regexes)
}

// GetDevicesByPhyTypeLastSeenContext - returns "last-seen" Devices of a given PHY type.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#phy-handlers.
func (cl *Client) GetDevicesByPhyTypeLastSeenContext(ctx context.Context,
	phyName string, timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(
			devicesByPhyTypeLastSeenURL,
			url.PathEscape(phyName),
			timestamp,
		),
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDevicesByPhyTypeLastSeen - returns "last-seen" Devices of a given PHY type.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#phy-handlers.
func (cl *Client) GetDevicesByPhyTypeLastSeen(
	phyName string, timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDevicesByPhyTypeLastSeenContext(context.Background(), phyName, timestamp, fields, regexes)
}

// GetDot11DeviceClientsContext - returns clients of the provided device. If the device is not
// a WiFi access point, then an empty list is returned.
func (cl *Client) GetDot11DeviceClientsContext(ctx context.Context, deviceKey string, fields FieldSimplification) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(devicesDot11DeviceClientsURL, deviceKey),
		&QueryOptions{Fields: fields},
	)
}

// GetDot11DeviceClients - returns clients of the provided device. If the device is not
// a WiFi access point, then an empty list is returned.
func (cl *Client) GetDot11DeviceClients(deviceKey string, fields FieldSimplification) ([]Device, error) {
	return cl.GetDot11DeviceClientsContext(context.Background(), deviceKey, fields)
}

// GetDot11AccessPointsContext - returns clients of the provided device. If the device is not
// a WiFi access point, then an empty list is returned.
func (cl *Client) GetDot11AccessPointsContext(ctx context.Context, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		devicesDot11AccessPointsURL,
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDot11AccessPoints - returns clients of the provided device. If the device is not
// a WiFi access point, then an empty list is returned.
func (cl *Client) GetDot11AccessPoints(fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDot11AccessPointsContext(context.Background(), fields, regexes)
}

// GetDot11AccessPointsLastSeenContext - returns clients of the provided device. If the device is not
// a WiFi access point, then an empty list is returned.
func (cl *Client) GetDot11AccessPointsLastSeenContext(ctx context.Context,
	timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(devicesDot11AccessPointsLastSeenURL, timestamp),
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDot11AccessPointsLastSeen - returns clients of the provided device. If the device is not
// a WiFi access point, then an empty list is returned.
func (cl *Client) GetDot11AccessPointsLastSeen(
	timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDot11AccessPointsLastSeenContext(context.Background(), timestamp, fields, regexes)
}

// GetDot11RelatedDevicesContext - returns clients of the provided device. If the device is not
// a WiFi access point, then an empty list is returned.
func (cl *Client) GetDot11RelatedDevicesContext(ctx context.Context,
	deviceKey string, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.getDevicesContext(
		ctx,
		fmt.Sprintf(devicesDot11RelatedDevicesURL, deviceKey),
		&QueryOptions{Fields: fields, RegexFilter: regexes},
	)
}

// GetDot11RelatedDevices - returns clients of the provided device. If the device is not
// a WiFi access point, then an empty list is returned.
func (cl *Client) GetDot11RelatedDevices(
	deviceKey string, timestamp int, fields FieldSimplification, regexes RegexFilters) ([]Device, error) {
	return cl.GetDot11RelatedDevicesContext(context.Background(), deviceKey, fields, regexes)
}
