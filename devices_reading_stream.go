package kismet

import "context"

type DevicesStreamer interface {
	Close() error
	GetData() (*Device, error)
	Next() bool
	Error() error
}

var _ DevicesStreamer = (*devicesStreamer)(nil)

//easyjson:skip
type devicesStreamer struct {
	*kismetStreamer
}

func newDevicesStream(ctx context.Context,
	cl *Client,
	method string, urlSuffix string,
	options interface{}) (DevicesStreamer, error) {
	ks, err := newStreamContext(ctx, cl, method, urlSuffix, options)
	if err != nil {
		return nil, err
	}

	return &devicesStreamer{kismetStreamer: ks}, nil
}

func (ds *devicesStreamer) GetData() (*Device, error) {
	var device Device

	if err := ds.kismetStreamer.GetData(&device); err != nil {
		return nil, err
	}

	return &device, nil
}
