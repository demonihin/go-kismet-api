package kismet

import "encoding/json"

/*
802.11 devices data structures.
*/

// Dot11AdvertisedSSIDDot11DCountryEnum - a country to which the detected SSID belongs:
// configured country code.
type Dot11AdvertisedSSIDDot11DCountryEnum string

const (
	Dot11CountryNoCountry Dot11AdvertisedSSIDDot11DCountryEnum = ""

	Dot11CountryChile                     Dot11AdvertisedSSIDDot11DCountryEnum = "CL"
	Dot11CountryChina                     Dot11AdvertisedSSIDDot11DCountryEnum = "CN"
	Dot11CountryColombia                  Dot11AdvertisedSSIDDot11DCountryEnum = "CO"
	Dot11CountryCostaRica                 Dot11AdvertisedSSIDDot11DCountryEnum = "CR"
	Dot11CountryCyprus                    Dot11AdvertisedSSIDDot11DCountryEnum = "CY"
	Dot11CountryCzechRepublic             Dot11AdvertisedSSIDDot11DCountryEnum = "CZ"
	Dot11CountryDenmark                   Dot11AdvertisedSSIDDot11DCountryEnum = "DK"
	Dot11CountryDominicanRepublic         Dot11AdvertisedSSIDDot11DCountryEnum = "DO"
	Dot11CountryEcuador                   Dot11AdvertisedSSIDDot11DCountryEnum = "EC"
	Dot11CountryEgypt                     Dot11AdvertisedSSIDDot11DCountryEnum = "EG"
	Dot11CountryElSalvador                Dot11AdvertisedSSIDDot11DCountryEnum = "SV"
	Dot11CountryEstonia                   Dot11AdvertisedSSIDDot11DCountryEnum = "EE"
	Dot11CountryFinland                   Dot11AdvertisedSSIDDot11DCountryEnum = "FI"
	Dot11CountryFrance                    Dot11AdvertisedSSIDDot11DCountryEnum = "FR"
	Dot11CountryGermany                   Dot11AdvertisedSSIDDot11DCountryEnum = "DE"
	Dot11CountryGreece                    Dot11AdvertisedSSIDDot11DCountryEnum = "GR"
	Dot11CountryGuatemala                 Dot11AdvertisedSSIDDot11DCountryEnum = "GT"
	Dot11CountryHonduras                  Dot11AdvertisedSSIDDot11DCountryEnum = "HN"
	Dot11CountryHongKong                  Dot11AdvertisedSSIDDot11DCountryEnum = "HK"
	Dot11CountryIceland                   Dot11AdvertisedSSIDDot11DCountryEnum = "IS"
	Dot11CountryIndia                     Dot11AdvertisedSSIDDot11DCountryEnum = "IN"
	Dot11CountryIndonesia                 Dot11AdvertisedSSIDDot11DCountryEnum = "ID"
	Dot11CountryIreland                   Dot11AdvertisedSSIDDot11DCountryEnum = "IE"
	Dot11CountryIslamicRepublicOfPakistan Dot11AdvertisedSSIDDot11DCountryEnum = "PK"
	Dot11CountryIsrael                    Dot11AdvertisedSSIDDot11DCountryEnum = "IL"
	Dot11CountryItaly                     Dot11AdvertisedSSIDDot11DCountryEnum = "IT"
	Dot11CountryJamaica                   Dot11AdvertisedSSIDDot11DCountryEnum = "JM"
	Dot11CountryJapan                     Dot11AdvertisedSSIDDot11DCountryEnum = "JP3"
	Dot11CountryJordan                    Dot11AdvertisedSSIDDot11DCountryEnum = "JO"
	Dot11CountryKenya                     Dot11AdvertisedSSIDDot11DCountryEnum = "KE"
	Dot11CountryKuwait                    Dot11AdvertisedSSIDDot11DCountryEnum = "KW"
	Dot11CountryLebanon                   Dot11AdvertisedSSIDDot11DCountryEnum = "LB"
	Dot11CountryLiechtenstein             Dot11AdvertisedSSIDDot11DCountryEnum = "LI"
	Dot11CountryLithuania                 Dot11AdvertisedSSIDDot11DCountryEnum = "LT"
	Dot11CountryLuxembourg                Dot11AdvertisedSSIDDot11DCountryEnum = "LU"
	Dot11CountryMauritius                 Dot11AdvertisedSSIDDot11DCountryEnum = "MU"
	Dot11CountryMexico                    Dot11AdvertisedSSIDDot11DCountryEnum = "MX"
	Dot11CountryMorocco                   Dot11AdvertisedSSIDDot11DCountryEnum = "MA"
	Dot11CountryNetherlands               Dot11AdvertisedSSIDDot11DCountryEnum = "NL"
	Dot11CountryNewZealand                Dot11AdvertisedSSIDDot11DCountryEnum = "NZ"
	Dot11CountryNorway                    Dot11AdvertisedSSIDDot11DCountryEnum = "NO"
	Dot11CountryOman                      Dot11AdvertisedSSIDDot11DCountryEnum = "OM"
	Dot11CountryPanama                    Dot11AdvertisedSSIDDot11DCountryEnum = "PA"
	Dot11CountryPeru                      Dot11AdvertisedSSIDDot11DCountryEnum = "PE"
	Dot11CountryPhilippines               Dot11AdvertisedSSIDDot11DCountryEnum = "PH"
	Dot11CountryPoland                    Dot11AdvertisedSSIDDot11DCountryEnum = "PL"
	Dot11CountryPortugal                  Dot11AdvertisedSSIDDot11DCountryEnum = "PT"
	Dot11CountryPuertoRico                Dot11AdvertisedSSIDDot11DCountryEnum = "PR"
	Dot11CountryQatar                     Dot11AdvertisedSSIDDot11DCountryEnum = "QA"
	Dot11CountryRepublicOfKoreaSouthKorea Dot11AdvertisedSSIDDot11DCountryEnum = "KR"
	Dot11CountryRomania                   Dot11AdvertisedSSIDDot11DCountryEnum = "RO"
	Dot11CountryRussia                    Dot11AdvertisedSSIDDot11DCountryEnum = "RU"
	Dot11CountrySaudiArabia               Dot11AdvertisedSSIDDot11DCountryEnum = "SA"
	Dot11CountrySerbiaandMontenegro       Dot11AdvertisedSSIDDot11DCountryEnum = "CS"
	Dot11CountrySingapore                 Dot11AdvertisedSSIDDot11DCountryEnum = "SG"
	Dot11CountrySlovakRepublic            Dot11AdvertisedSSIDDot11DCountryEnum = "SK"
	Dot11CountrySlovenia                  Dot11AdvertisedSSIDDot11DCountryEnum = "SI"
	Dot11CountrySouthAfrica               Dot11AdvertisedSSIDDot11DCountryEnum = "ZA"
	Dot11CountrySpain                     Dot11AdvertisedSSIDDot11DCountryEnum = "ES"
	Dot11CountrySriLanka                  Dot11AdvertisedSSIDDot11DCountryEnum = "LK"
	Dot11CountrySwitzerland               Dot11AdvertisedSSIDDot11DCountryEnum = "CH"
	Dot11CountryTaiwan                    Dot11AdvertisedSSIDDot11DCountryEnum = "TW"
	Dot11CountryThailand                  Dot11AdvertisedSSIDDot11DCountryEnum = "TH"
	Dot11CountryTrinidadandTobago         Dot11AdvertisedSSIDDot11DCountryEnum = "TT"
	Dot11CountryTunisia                   Dot11AdvertisedSSIDDot11DCountryEnum = "TN"
	Dot11CountryTurkey                    Dot11AdvertisedSSIDDot11DCountryEnum = "TR"
	Dot11CountryUkraine                   Dot11AdvertisedSSIDDot11DCountryEnum = "UA"
	Dot11CountryUnitedArabEmirates        Dot11AdvertisedSSIDDot11DCountryEnum = "AE"
	Dot11CountryUnitedKingdom             Dot11AdvertisedSSIDDot11DCountryEnum = "GB"
	Dot11CountryUnitedStates              Dot11AdvertisedSSIDDot11DCountryEnum = "US"
	Dot11CountryUruguay                   Dot11AdvertisedSSIDDot11DCountryEnum = "UY"
	Dot11CountryVenezuela                 Dot11AdvertisedSSIDDot11DCountryEnum = "VE"
	Dot11CountryVietnam                   Dot11AdvertisedSSIDDot11DCountryEnum = "VN"
)

// Dot11AdvertisedSSIDHTModeEnum - HT mode.
type Dot11AdvertisedSSIDHTModeEnum string

const (
	Dot11AdvertisedSSIDHTMode Dot11AdvertisedSSIDHTModeEnum = ""
	Ht20                      Dot11AdvertisedSSIDHTModeEnum = "HT20"
	Ht40                      Dot11AdvertisedSSIDHTModeEnum = "HT40+"
	Ht80                      Dot11AdvertisedSSIDHTModeEnum = "HT80"
)

type Dot11AdvertisedSSIDWpsUUIDE string

type Dot11ProbedSSIDBssid string

type Dot11ProbedSSIDWpsUUIDEEnum string

// DeviceBaseCryptEnum - Detected encryption type.
type DeviceBaseCryptEnum string

const (
	DeviceBaseCrypt DeviceBaseCryptEnum = ""
	Wpa2Psk         DeviceBaseCryptEnum = "WPA2-PSK"
	Wpa3Sae         DeviceBaseCryptEnum = "WPA3-SAE"
	Wpa3Transition  DeviceBaseCryptEnum = "WPA3-TRANSITION"
)

// Dot11Device - a 802.11 Device.
//easyjson:json
type Dot11Device struct {
	SSIDBeaconPacket *Dot11Packet                  `json:"dot11.device.ssid_beacon_packet,omitempty"`
	PmkidPacket      *Dot11Packet                  `json:"dot11.device.pmkid_packet,omitempty"`
	WPAHandshakeList []Dot11DeviceWPAHandshakeList `json:"dot11.device.wpa_handshake_list,omitempty"`
	// Datasize - data in bytes.
	Datasize uint64 `json:"dot11.device.datasize"`
	// NumRetries - number of retried packets.
	NumRetries uint64                          `json:"dot11.device.num_retries"`
	ClientMap  map[string]Dot11DeviceClientMap `json:"dot11.device.client_map,omitempty"`
	// NumFragments - number of fragmented packets.
	NumFragments uint64 `json:"dot11.device.num_fragments"`
	// ClientDisconnectsLast - client disconnects last message.
	ClientDisconnectsLast uint64 `json:"dot11.device.client_disconnects_last"`
	// NumRespondedSsids - number of responded SSIDs.
	NumRespondedSsids uint64 `json:"dot11.device.num_responded_ssids"`
	// BSSTimestamp - last BSS timestamp.
	BSSTimestamp uint64 `json:"dot11.device.bss_timestamp"`
	// Typeset - bitset of device type.
	Typeset uint64 `json:"dot11.device.typeset"`
	// ClientDisconnects - client disconnects message count.
	ClientDisconnects uint64 `json:"dot11.device.client_disconnects"`
	// NumAssociatedClients - number of associated clients.
	NumAssociatedClients uint64 `json:"dot11.device.num_associated_clients"`
	// AdvertisedSSIDMap - advertised SSIDs.
	AdvertisedSSIDMap []Dot11DeviceLastBeaconedSSIDRecordElement `json:"dot11.device.advertised_ssid_map,omitempty"`
	// NumProbedSsids - number of probed SSIDs.
	NumProbedSsids uint64 `json:"dot11.device.num_probed_ssids"`
	// NumAdvertisedSsids - number of advertised SSIDs.
	NumAdvertisedSsids uint64                                     `json:"dot11.device.num_advertised_ssids"`
	RespondedSSIDMap   []Dot11DeviceLastBeaconedSSIDRecordElement `json:"dot11.device.responded_ssid_map,omitempty"`
	// WPAPresentHandshake - handshake sequences seen (bitmask).
	WPAPresentHandshake uint8 `json:"dot11.device.wpa_present_handshake"`
	// NumClientAps - number of APs connected to.
	NumClientAps int64 `json:"dot11.device.num_client_aps"`
	// LastSequence - last sequence number.
	LastSequence uint64 `json:"dot11.device.last_sequence"`
	// DatasizeRetry - retried data in bytes.
	DatasizeRetry uint64 `json:"dot11.device.datasize_retry"`
	// LastBeaconTimestamp - unix timestamp of last beacon frame.
	LastBeaconTimestamp uint64 `json:"dot11.device.last_beacon_timestamp"`
	// MinTxPower - Minimum advertised TX power.
	MinTxPower uint8 `json:"dot11.device.min_tx_power"`
	// WpsM3Count - WPS M3 message count.
	WpsM3Count uint64 `json:"dot11.device.wps_m3_count"`
	// MaxTxPower - Maximum advertised TX power.
	MaxTxPower uint8 `json:"dot11.device.max_tx_power"`
	// WpsM3Last - WPS M3 last message.
	WpsM3Last uint64 `json:"dot11.device.wps_m3_last"`
	// LinkMeasurementCapable - Advertised link measurement client capability.
	LinkMeasurementCapable uint8 `json:"dot11.device.link_measurement_capable"`
	// NeighborReportCapable - Advertised neighbor report capability.
	NeighborReportCapable uint8 `json:"dot11.device.neighbor_report_capable"`
	// BeaconFingerprint - Beacon fingerprint.
	BeaconFingerprint uint32 `json:"dot11.device.beacon_fingerprint"`
	// ProbeFingerprint - Probe (Client->AP) fingerprint.
	ProbeFingerprint uint32 `json:"dot11.device.probe_fingerprint"`
	// ResponseFingerprint - Response (AP->Client) fingerprint.
	ResponseFingerprint uint64 `json:"dot11.device.response_fingerprint"`
	// LastBssid - mac_addr, last BSSID.
	LastBssid *string `json:"dot11.device.last_bssid,omitempty"`
	// LastBeaconedSSIDRecord - last beaconed ssid, complete record.
	LastBeaconedSSIDRecord *Dot11DeviceLastBeaconedSSIDRecordElement `json:"dot11.device.last_beaconed_ssid_record,omitempty"`
	// AssociatedClientMap - map[macaddr, x], associated clients.
	AssociatedClientMap  *Dot11DeviceAssociatedClientMap        `json:"dot11.device.associated_client_map,omitempty"`
	LastProbedSSIDRecord *Dot11DeviceLastProbedSSIDRecordClass  `json:"dot11.device.last_probed_ssid_record,omitempty"`
	ProbedSSIDMap        []Dot11DeviceLastProbedSSIDRecordClass `json:"dot11.device.probed_ssid_map,omitempty"`
	WPANonceList         []Dot11DeviceWPANonceList              `json:"dot11.device.wpa_nonce_list,omitempty"`
	WPAAnonceList        []Dot11DeviceWPANonceList              `json:"dot11.device.wpa_anonce_list,omitempty"`
}

// Dot11Packet - dot11 packet.
//easyjson:skip
type Dot11Packet struct {
	PacketData   []byte   `json:"kismet.packet.data"`
	PacketSource int64    `json:"kismet.packet.source"`
	PacketDlt    int64    `json:"kismet.packet.dlt"`
	PacketTS     TimeUSec `json:"kismet.packet.ts"`
}

// dot11packetRaw - Dot11Packet before conversion.
//easyjson:json
type dot11packetRaw struct {
	PacketData   []byte `json:"kismet.packet.data"`
	PacketSource int64  `json:"kismet.packet.source"`
	PacketDlt    int64  `json:"kismet.packet.dlt"`
	PacketTSUsec int64  `json:"kismet.packet.ts_usec"`
	PacketTSSec  int64  `json:"kismet.packet.ts_sec"`
}

func (d11pkt *Dot11Packet) UnmarshalJSON(raw []byte) error {
	var parsedRaw dot11packetRaw

	if err := json.Unmarshal(raw, &parsedRaw); err != nil {
		return err
	}

	d11pkt.PacketData = parsedRaw.PacketData
	d11pkt.PacketSource = parsedRaw.PacketSource
	d11pkt.PacketDlt = parsedRaw.PacketDlt
	d11pkt.PacketTS = TimeUSec{
		Microseconds: int32(parsedRaw.PacketTSUsec),
		Seconds:      parsedRaw.PacketTSSec,
	}

	return nil
}

// Dot11DeviceWPANonceList - WPA Nonce data.
//easyjson:json
type Dot11DeviceWPANonceList struct {
	EapolNonceNonce         string  `json:"dot11.eapol.nonce.nonce"`
	EapolNonceInstall       int64   `json:"dot11.eapol.nonce.install"`
	EapolNonceReplayCounter int64   `json:"dot11.eapol.nonce.replay_counter"`
	EapolNonceMessageNum    int64   `json:"dot11.eapol.nonce.message_num"`
	EapolNonceTimestamp     float64 `json:"dot11.eapol.nonce.timestamp"`
}

type Dot11EapolRsnPmkidEnum string

// Dot11DeviceWPAHandshakeList - WPA handshakes.
//easyjson:json
type Dot11DeviceWPAHandshakeList struct {
	EapolPacket        Dot11Packet            `json:"dot11.eapol.packet"`
	EapolRsnPmkid      Dot11EapolRsnPmkidEnum `json:"dot11.eapol.rsn_pmkid"`
	EapolNonce         string                 `json:"dot11.eapol.nonce"`
	EapolInstall       int64                  `json:"dot11.eapol.install"`
	EapolReplayCounter int64                  `json:"dot11.eapol.replay_counter"`
	EapolMessageNum    int64                  `json:"dot11.eapol.message_num"`
	EapolDirection     int64                  `json:"dot11.eapol.direction"`
	EapolTimestamp     float64                `json:"dot11.eapol.timestamp"`
}

// Dot11DeviceLastBeaconedSSIDRecordElement - an announced SSID.
//easyjson:json
type Dot11DeviceLastBeaconedSSIDRecordElement struct {
	// SSIDCiscoClientMfp - Cisco client management frame protection.
	SSIDCiscoClientMfp uint8 `json:"dot11.advertisedssid.cisco_client_mfp"`
	// SSIDCcxTxpower - Cisco CCX advertised TX power (dBm).
	SSIDCcxTxpower uint8 `json:"dot11.advertisedssid.ccx_txpower"`
	// SSIDDot11EChannelUtilizationPerc - double, 802.11e QBSS reported channel utilization, as percentage.
	SSIDDot11EChannelUtilizationPerc float64 `json:"dot11.advertisedssid.dot11e_channel_utilization_perc"`
	// SSIDDot11EQbssStations - 802.11e QBSS station count.
	SSIDDot11EQbssStations uint16 `json:"dot11.advertisedssid.dot11e_qbss_stations"`
	// SSIDDot11EQbss - SSID advertises 802.11e QBSS.
	SSIDDot11EQbss uint8 `json:"dot11.advertisedssid.dot11e_qbss"`
	// SSIDDot11RMobilityDomainID - advertised dot11r mobility domain id.
	SSIDDot11RMobilityDomainID uint16 `json:"dot11.advertisedssid.dot11r_mobility_domain_id"`
	// SSIDDot11RMobility - advertised dot11r mobility support.
	SSIDDot11RMobility uint8                        `json:"dot11.advertisedssid.dot11r_mobility"`
	SSIDWpsUUIDE       *Dot11AdvertisedSSIDWpsUUIDE `json:"dot11.advertisedssid.wps_uuid_e,omitempty"`
	// SSIDFirstTime - first time seen, timestamp.
	SSIDFirstTime uint64 `json:"dot11.advertisedssid.first_time"`
	// SSIDHTCenter2 - HT/VHT Center Frequency (secondary, for 80+80 Wave2).
	SSIDHTCenter2 uint64 `json:"dot11.advertisedssid.ht_center_2"`
	// SSIDHTCenter1 - HT/VHT Center Frequency (primary).
	SSIDHTCenter1 uint64 `json:"dot11.advertisedssid.ht_center_1"`
	// SSIDHTMode - HT (11n or 11ac) mode.
	SSIDHTMode Dot11AdvertisedSSIDHTModeEnum `json:"dot11.advertisedssid.ht_mode"`
	// SSIDChannel - channel.
	SSIDChannel string `json:"dot11.advertisedssid.channel"`
	// SSIDProbeResponse - ssid advertised via probe response.
	SSIDProbeResponse uint8 `json:"dot11.advertisedssid.probe_response"`
	// SSIDBeacon - ssid advertised via beacon.
	SSIDBeacon uint8 `json:"dot11.advertisedssid.beacon"`
	// SSIDSSIDHash - hashed key of the SSID+Length.
	SSIDSSIDHash uint64 `json:"dot11.advertisedssid.ssid_hash"`
	// SSIDWpsConfigMethods - bitfield wps config methods.
	SSIDWpsConfigMethods uint16 `json:"dot11.advertisedssid.wps_config_methods"`
	// SSIDSSIDlen - beaconed ssid string length (original bytes)
	SSIDSSIDlen uint32 `json:"dot11.advertisedssid.ssidlen"`
	// SSIDWpsState - bitfield wps state.
	SSIDWpsState uint32 `json:"dot11.advertisedssid.wps_state"`
	// SSIDSSID - beaconed ssid string (sanitized).
	SSIDSSID string `json:"dot11.advertisedssid.ssid"`
	// SSIDWpsVersion - WPS version.
	SSIDWpsVersion uint8 `json:"dot11.advertisedssid.wps_version"`
	// SSIDLastTime - last time seen.
	SSIDLastTime uint64 `json:"dot11.advertisedssid.last_time"`
	// SSIDCloaked - SSID is hidden / cloaked.
	SSIDCloaked uint8 `json:"dot11.advertisedssid.cloaked"`
	// SSIDCryptSet - bitfield of encryption options.
	SSIDCryptSet uint64 `json:"dot11.advertisedssid.crypt_set"`
	// SSIDMaxrate - advertised maximum rate.
	SSIDMaxrate float64 `json:"dot11.advertisedssid.maxrate"`
	// SSIDBeaconrate -  beacon rate.
	SSIDBeaconrate uint32 `json:"dot11.advertisedssid.beaconrate"`
	// SSIDBeaconsSEC - beacons seen in past second.
	SSIDBeaconsSEC uint64 `json:"dot11.advertisedssid.beacons_sec"`
	// SSIDIetagChecksum - checksum of all ie tags.
	SSIDIetagChecksum uint32 `json:"dot11.advertisedssid.ietag_checksum"`
	// SSIDWPAMfpRequired - WPA management protection required.
	SSIDWPAMfpRequired uint8 `json:"dot11.advertisedssid.wpa_mfp_required"`
	// SSIDWPAMfpSupported - WPA management protection supported.
	SSIDWPAMfpSupported uint8 `json:"dot11.advertisedssid.wpa_mfp_supported"`
	// SSIDDot11DCountry - 802.11d country.
	SSIDDot11DCountry   *Dot11AdvertisedSSIDDot11DCountryEnum `json:"dot11.advertisedssid.dot11d_country,omitempty"`
	SSIDWpsSerialNumber *string                               `json:"dot11.advertisedssid.wps_serial_number,omitempty"`
	SSIDWpsModelNumber  *string                               `json:"dot11.advertisedssid.wps_model_number,omitempty"`
	SSIDWpsModelName    *string                               `json:"dot11.advertisedssid.wps_model_name,omitempty"`
	SSIDWpsDeviceName   *string                               `json:"dot11.advertisedssid.wps_device_name,omitempty"`
	SSIDWpsManuf        *string                               `json:"dot11.advertisedssid.wps_manuf,omitempty"`
}

// Dot11DeviceAssociatedClientMap - clients connected to a 802.11 Device.
// key - 802.11 Device, value - client's ID.
type Dot11DeviceAssociatedClientMap map[string]string

// Dot11DeviceClientMap - associated clients.
//easyjson:json
type Dot11DeviceClientMap struct {
	// NumRetries - number of retried packets.
	NumRetries uint64 `json:"dot11.client.num_retries"`
	// NumFragments - number of fragmented packets.
	NumFragments uint64 `json:"dot11.client.num_fragments"`
	// DatasizeRetry - retry data in bytes.
	DatasizeRetry uint64 `json:"dot11.client.datasize_retry"`
	// Datasize - data in bytes.
	Datasize uint64 `json:"dot11.client.datasize"`
	// Bssid - mac_addr, bssid.
	Bssid string `json:"dot11.client.bssid"`
	// BssidKey - devicekey, key of BSSID record.
	BssidKey string `json:"dot11.client.bssid_key"`
	// FirstTime - first time seen.
	FirstTime int64 `json:"dot11.client.first_time"`
	// LastTime - last time seen.
	LastTime int64 `json:"dot11.client.last_time"`
	// Type - type of client.
	Type uint32 `json:"dot11.client.type"`
	// TxCryptset - bitset of transmitted encryption.
	TxCryptset uint64 `json:"dot11.client.tx_cryptset"`
	// RxCryptset - bitset of received encryption.
	RxCryptset uint64 `json:"dot11.client.rx_cryptset"`
	// Decrypted - client decrypted.
	Decrypted uint8 `json:"dot11.client.decrypted"`
}

// Dot11DeviceLastProbedSSIDRecordClass - SSID information.
//easyjson:json
type Dot11DeviceLastProbedSSIDRecordClass struct {
	DWpsUUIDE               Dot11ProbedSSIDWpsUUIDEEnum `json:"dot11.probedssid.wps_uuid_e"`
	DWpsModelNumber         *string                     `json:"dot11.probedssid.wps_model_number,omitempty"`
	DWpsModelName           *string                     `json:"dot11.probedssid.wps_model_name,omitempty"`
	DWpsManuf               *string                     `json:"dot11.probedssid.wps_manuf,omitempty"`
	DWpsConfigMethods       int64                       `json:"dot11.probedssid.wps_config_methods"`
	DWpsState               int64                       `json:"dot11.probedssid.wps_state"`
	DSSID                   string                      `json:"dot11.probedssid.ssid"`
	DSsidlen                int64                       `json:"dot11.probedssid.ssidlen"`
	DBssid                  Dot11ProbedSSIDBssid        `json:"dot11.probedssid.bssid"`
	DFirstTime              int64                       `json:"dot11.probedssid.first_time"`
	DLastTime               int64                       `json:"dot11.probedssid.last_time"`
	DDot11RMobility         int64                       `json:"dot11.probedssid.dot11r_mobility"`
	DDot11RMobilityDomainID int64                       `json:"dot11.probedssid.dot11r_mobility_domain_id"`
	DCryptSet               int64                       `json:"dot11.probedssid.crypt_set"`
	DWPAMfpRequired         int64                       `json:"dot11.probedssid.wpa_mfp_required"`
	DWPAMfpSupported        int64                       `json:"dot11.probedssid.wpa_mfp_supported"`
	DWpsVersion             int64                       `json:"dot11.probedssid.wps_version"`
}
