package kismet

/*
Bluetooth device data structures.
*/

// BluetoothDevice - a Bluetooth device.
//easyjson:json
type BluetoothDevice struct {
	// Type - bt device type.
	Type uint8 `json:"bluetooth.device.type"`
	// Pathloss - signal pathloss.
	Pathloss int16 `json:"bluetooth.device.pathloss"`
	// Txpower - advertised transmit power.
	Txpower int16 `json:"bluetooth.device.txpower"`
	// ServiceDataBytes - per-service result bytes.
	ServiceDataBytes BluetoothDeviceServiceDataBytes `json:"bluetooth.device.service_data_bytes"`
	// ScanDataBytes - scan result bytes.
	ScanDataBytes []byte `json:"bluetooth.device.scan_data_bytes"`
	// SolicitationUUIDVec - advertised solicitation UUIDs.
	SolicitationUUIDVec []interface{} `json:"bluetooth.device.solicitation_uuid_vec"`
	// ServiceUUIDVec - advertised service UUIDs.
	ServiceUUIDVec []interface{} `json:"bluetooth.device.service_uuid_vec"`
}

// BluetoothDeviceServiceDataBytes - service data bytes.
//easyjson:json
type BluetoothDeviceServiceDataBytes struct {
}
