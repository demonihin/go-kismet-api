package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestDatasourcesSuite struct {
	suite.Suite
}

func (t *TestDatasourcesSuite) TestGetDatasourcesSupportedTypesContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	datasources, err := cl.GetDatasourcesSupportedTypesContext(context.Background())
	t.NoError(err)
	t.NotNil(datasources)

	out, _ := json.MarshalIndent(datasources, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDatasourcesSuite) TestGetDatasourcesDefaultsContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	defaults, err := cl.GetDatasourcesDefaultsContext(context.Background())
	t.NoError(err)
	t.NotNil(defaults)

	out, _ := json.MarshalIndent(defaults, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDatasourcesSuite) TestGetDatasourcesAllContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	datasources, err := cl.GetDatasourcesAllContext(context.Background())
	t.NoError(err)
	t.NotNil(datasources)

	out, _ := json.MarshalIndent(datasources, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDatasourcesSuite) TestGetDatasourceContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	datasource, err := cl.GetDatasourceContext(context.Background(), "91DD0AE4-0000-0000-0000-A0C589C5FF34")
	t.NoError(err)
	t.NotNil(datasource)

	out, _ := json.MarshalIndent(datasource, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDatasourcesSuite) TestGetDatasourcesInterfacesContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	interfaces, err := cl.GetDatasourcesInterfacesContext(context.Background())
	t.NoError(err)
	t.NotNil(interfaces)

	out, _ := json.MarshalIndent(interfaces, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDatasourcesSuite) TestAddDatasourceContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.AddDatasourceContext(context.Background(), "hci0")
	t.NoError(err)
}

func (t *TestDatasourcesSuite) TestSetDatasourceChannelContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.SetDatasourceFixedChannelContext(context.Background(), "5FE308BD-0000-0000-0000-00E04C53B636", "1")
	t.NoError(err)
}

func (t *TestDatasourcesSuite) TestSetDatasourceChannelHoppingContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	var (
		shuffle = false
		rate    = 2
	)

	err = cl.SetDatasourceChannelHoppingContext(context.Background(),
		"5FE308BD-0000-0000-0000-00E04C53B636", []string{"1", "10"}, &rate, &shuffle)
	t.NoError(err)
}

func (t *TestDatasourcesSuite) TestSetDatasourceChannelHoppingEnableContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.SetDatasourceChannelHoppingEnableContext(context.Background(), "5FE308BD-0000-0000-0000-00E04C53B636")
	t.NoError(err)
}

func (t *TestDatasourcesSuite) TestOpenDatasourceContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.OpenDatasourceContext(context.Background(), "5FE308BD-0000-0000-0000-00E04C53B636")
	t.NoError(err)
}

func (t *TestDatasourcesSuite) TestCloseDatasourceContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.CloseDatasourceContext(context.Background(), "5FE308BD-0000-0000-0000-00E04C53B636")
	t.NoError(err)
}

func (t *TestDatasourcesSuite) TestPauseDatasourceContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.PauseDatasourceContext(context.Background(), "5FE308BD-0000-0000-0000-00E04C53B636")
	t.NoError(err)
}

func (t *TestDatasourcesSuite) TestResumeDatasourceContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.ResumeDatasourceContext(context.Background(), "5FE308BD-0000-0000-0000-00E04C53B636")
	t.NoError(err)
}

func TestTunTestDatasourcesSuite(t *testing.T) {
	suite.Run(t, &TestDatasourcesSuite{})
}
