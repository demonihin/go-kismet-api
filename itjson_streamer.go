package kismet

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// streamBufferSize - I checked that some Kismet reported items have about 9KB size
// so 16KB should be enough to preallocate for bufio.
const streamBufferSize = 16 * 1024 * 1024

type streamer interface {
	Close() error
	GetData(value interface{}) error
	Next() bool
	Error() error
}

//easyjson:skip
type kismetStreamer struct {
	buffer *bufio.Reader

	sourceCloser func() error

	// currentError - could be set by Next call and then it is passed to the
	// caller of GetData if not nil.
	currentError  error
	currentRecord []byte
}

var _ streamer = (*kismetStreamer)(nil)

func newStreamContext(ctx context.Context,
	cl *Client,
	method string, urlSuffix string,
	options interface{}) (*kismetStreamer, error) {
	// Command.
	reqBody, err := json.Marshal(options)
	if err != nil {
		return nil, err
	}

	var data = &url.Values{}
	data.Set("json", string(reqBody))
	var encodedData = data.Encode()

	// Request.
	req, err := http.NewRequestWithContext(ctx, method,
		cl.serverURL+urlSuffix, strings.NewReader(encodedData))
	if err != nil {
		return nil, err
	}

	// Authentication.
	req.AddCookie(&http.Cookie{
		Name:  "KISMET",
		Value: cl.apiToken,
	})

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(encodedData)))

	// Send.
	resp, err := cl.httpClient.Do(req) //nolint:bodyclose // The body must be closed by the stream.Close method.
	if err != nil {
		return nil, err
	}

	// Check response code.
	if !(resp.StatusCode >= 200 && resp.StatusCode < 300) {
		bts, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		return nil, fmt.Errorf("%w: status code is %d, server message: %s", ErrRequestFailed, resp.StatusCode, string(bts))
	}

	// Init buffered reading.
	var buffer = bufio.NewReaderSize(resp.Body, streamBufferSize)

	return &kismetStreamer{
		buffer:       buffer,
		sourceCloser: resp.Body.Close,
	}, nil
}

func (ks *kismetStreamer) Close() error {
	return ks.sourceCloser()
}

func (ks *kismetStreamer) GetData(value interface{}) error {
	if ks.currentError != nil {
		return ks.currentError
	}

	return json.Unmarshal(ks.currentRecord, value)
}

func (ks *kismetStreamer) Next() bool {
	data, err := ks.buffer.ReadBytes('\n')
	if err != nil {
		if err == io.EOF {
			ks.currentRecord = data
			return false
		}

		ks.currentError = err
		return false
	}

	ks.currentRecord = data
	return true
}

func (ks *kismetStreamer) Error() error {
	return ks.currentError
}
