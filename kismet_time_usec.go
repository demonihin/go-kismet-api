package kismet

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"
)

// TimeUSec - Kismet may send timestamps in microsecond precision format
// i.e. {{ seconds }}.{{ microseconds }} in Alerts, for example.
// The code below implements the format support.
//easyjson:skip
type TimeUSec struct {
	Microseconds int32
	Seconds      int64
}

var (
	_ json.Marshaler   = (*TimeUSec)(nil)
	_ json.Unmarshaler = (*TimeUSec)(nil)
	_ fmt.Stringer     = (*TimeUSec)(nil)
)

var ErrTimeUSecUnsupportedFormat = errors.New("unsupported TimeUSec format")

func (t *TimeUSec) MarshalJSON() ([]byte, error) {
	if t.Microseconds == 0 {
		return []byte(strconv.FormatInt(t.Seconds, 10)), nil
	}

	var bb = &bytes.Buffer{}

	bb.WriteString(strconv.FormatInt(t.Seconds, 10))
	bb.WriteByte('.')
	bb.WriteString(strconv.FormatInt(int64(t.Microseconds), 10))

	return bb.Bytes(), nil
}

func (t *TimeUSec) UnmarshalJSON(raw []byte) error {
	splits := bytes.Split(raw, []byte{'.'})

	// The number has {{seconds}}.{{microseconds}} format and the microseconds part is optional.

	switch len(splits) {
	case 1: // 1 means that the number has only "seconds" part.
		secs, err := strconv.ParseInt(string(splits[0]), 10, 64)
		if err != nil {
			return err
		}

		t.Seconds = secs

		return nil
	case 2: //nolint:gomnd // full seconds and microseconds format.
		secs, err := strconv.ParseInt(string(splits[0]), 10, 64)
		if err != nil {
			return err
		}

		t.Seconds = secs

		uSecs, err := strconv.ParseInt(string(splits[1]), 10, 64)
		if err != nil {
			return err
		}

		t.Microseconds = int32(uSecs)

		return nil
	}

	return fmt.Errorf("%w: %q can not be parsed", ErrTimeUSecUnsupportedFormat, string(raw))
}

func (t *TimeUSec) String() string {
	if t.Microseconds == 0 {
		return strconv.FormatInt(t.Seconds, 10)
	}

	return fmt.Sprintf("%d.%d", t.Seconds, t.Microseconds)
}

// Time - converts TimeUSec to go stdlib time.Time.
func (t *TimeUSec) Time() time.Time {
	return time.Unix(t.Seconds, int64(t.Microseconds)*1000) //nolint:gomnd // function receives seconds and nanoseconds.
}
