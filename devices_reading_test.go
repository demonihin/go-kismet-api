package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type TestDevicesReadingSuite struct {
	suite.Suite
}

func (t *TestDevicesReadingSuite) TestGetDevicesLastSeenContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDevicesLastSeenContext(context.Background(), -2000, nil, nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestStreamDevicesLastSeenContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.StreamDevicesLastSeenContext(context.Background(), -200, nil, nil)
	t.NoError(err)
	t.NotNil(devices)

	// Stream.
	for devices.Next() {
		dev, err := devices.GetData()
		t.NoError(err)

		out, _ := json.MarshalIndent(dev, "", "  ")
		fmt.Println(string(out))
	}

	t.NoError(devices.Error())
}

func (t *TestDevicesReadingSuite) TestGetDeviceByKeyContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDeviceByKeyContext(context.Background(), "B603E01100000000_58356CE5B754", nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestGetDevicesMultipleByKeysContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDevicesMultipleByKeysContext(context.Background(), []string{"B603E01100000000_58356CE5B754"}, nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestGetDevicesMultipleByKeysDictContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDevicesMultipleByKeysDictContext(context.Background(), []string{"B603E01100000000_58356CE5B754"}, nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestGetDevicesByMacContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDevicesByMacContext(context.Background(), "8C:79:F5:A6:96:F3", nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestGetDevicesMultipleByMacContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDevicesMultipleByMacContext(context.Background(), []string{"8C:79:F5:A6:96:F3", "ED:93:9F:B2:25:01"}, nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestGetDot11DeviceClientsContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDot11DeviceClientsContext(context.Background(), "4202770D00000000_378E907D9A20", nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestGetDot11AccessPointsContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDot11AccessPointsContext(context.Background(), nil, nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestGetDot11AccessPointsLastSeenContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDot11AccessPointsLastSeenContext(context.Background(), int(time.Now().Add(-1*time.Second).Unix()), nil, nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func (t *TestDevicesReadingSuite) TestGetDot11RelatedDevicesContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	devices, err := cl.GetDot11RelatedDevicesContext(context.Background(), "4202770D00000000_378E907D9A20", nil, nil)
	t.NoError(err)
	t.NotNil(devices)

	out, _ := json.MarshalIndent(devices, "", "  ")
	fmt.Println(string(out))
}

func TestRunDevicesReadingSuite(t *testing.T) {
	suite.Run(t, &TestDevicesReadingSuite{})
}
