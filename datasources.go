package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
)

const (
	datasourcesTypesAllURL            = "/datasource/types.json"
	datasourcesDefaultsURL            = "/datasource/defaults.json"
	datasourcesAllURL                 = "/datasource/all_sources.json"
	datasourceURL                     = "/datasource/by-uuid/%s/source.json"
	datasourcesInterfacesAllURL       = "/datasource/list_interfaces.json"
	datasourceAddCmdURL               = "/datasource/add_source.cmd"
	datasourceSetChannelCmdURL        = "/datasource/by-uuid/%s/set_channel.cmd"
	datasourceSetChannelHoppingCmdURL = "/datasource/by-uuid/%s/set_hop.cmd"
	datasourceCloseSourceCmdURL       = "/datasource/by-uuid/%s/close_source.cmd"
	datasourceOpenSourceCmdURL        = "/datasource/by-uuid/%s/open_source.cmd"
	datasourcePauseSourceCmdURL       = "/datasource/by-uuid/%s/pause_source.cmd"
	datasourceResumeSourceCmdURL      = "/datasource/by-uuid/%s/resume_source.cmd"
)

// DatasourceTypeDriver - capture driver information.
//easyjson:skip
type DatasourceTypeDriver struct {
	HopCapable     bool   `json:"kismet.datasource_driver.hop_capable"`
	TuningCapable  bool   `json:"kismet.datasource.driver.tuning_capable"`
	PassiveCapable bool   `json:"kismet.datasource.driver.passive_capable"`
	RemoteCapable  bool   `json:"kismet.datasource.driver.remote_capable"`
	LocalIPC       bool   `json:"kismet.datasource.driver.local_ipc"`
	LocalCapable   bool   `json:"kismet.datasource.driver.local_capable"`
	ListIPC        bool   `json:"kismet.datasource.driver.list_ipc"`
	ListCapable    bool   `json:"kismet.datasource.driver.list_capable"`
	ProbeIPC       bool   `json:"kismet.datasource.driver.probe_ipc"`
	ProbeCapable   bool   `json:"kismet.datasource.driver.probe_capable"`
	Description    string `json:"kismet.datasource.driver.description"`
	Type           string `json:"kismet.datasource.driver.type"`
}

var _ json.Unmarshaler = (*DatasourceTypeDriver)(nil)

// datasourceTypeDriverRaw - raw datasourceTypeDriver.
//easyjson:json
type datasourceTypeDriverRaw struct {
	DatasourceDriverHopCapable     int64  `json:"kismet.datasource_driver.hop_capable"`
	DatasourceDriverTuningCapable  int64  `json:"kismet.datasource.driver.tuning_capable"`
	DatasourceDriverPassiveCapable int64  `json:"kismet.datasource.driver.passive_capable"`
	DatasourceDriverRemoteCapable  int64  `json:"kismet.datasource.driver.remote_capable"`
	DatasourceDriverLocalIPC       int64  `json:"kismet.datasource.driver.local_ipc"`
	DatasourceDriverLocalCapable   int64  `json:"kismet.datasource.driver.local_capable"`
	DatasourceDriverListIPC        int64  `json:"kismet.datasource.driver.list_ipc"`
	DatasourceDriverListCapable    int64  `json:"kismet.datasource.driver.list_capable"`
	DatasourceDriverProbeIPC       int64  `json:"kismet.datasource.driver.probe_ipc"`
	DatasourceDriverProbeCapable   int64  `json:"kismet.datasource.driver.probe_capable"`
	DatasourceDriverDescription    string `json:"kismet.datasource.driver.description"`
	DatasourceDriverType           string `json:"kismet.datasource.driver.type"`
}

func (kdt *DatasourceTypeDriver) UnmarshalJSON(raw []byte) error {
	var rawParsed datasourceTypeDriverRaw

	if err := json.Unmarshal(raw, &rawParsed); err != nil {
		return err
	}

	kdt.HopCapable = rawParsed.DatasourceDriverHopCapable == 1
	kdt.TuningCapable = rawParsed.DatasourceDriverTuningCapable == 1
	kdt.PassiveCapable = rawParsed.DatasourceDriverPassiveCapable == 1
	kdt.RemoteCapable = rawParsed.DatasourceDriverRemoteCapable == 1
	kdt.LocalIPC = rawParsed.DatasourceDriverLocalIPC == 1
	kdt.LocalCapable = rawParsed.DatasourceDriverLocalCapable == 1
	kdt.ListIPC = rawParsed.DatasourceDriverListIPC == 1
	kdt.ListCapable = rawParsed.DatasourceDriverListCapable == 1
	kdt.ProbeIPC = rawParsed.DatasourceDriverProbeIPC == 1
	kdt.ProbeCapable = rawParsed.DatasourceDriverProbeCapable == 1

	kdt.Description = rawParsed.DatasourceDriverDescription
	kdt.Type = rawParsed.DatasourceDriverType

	return nil
}

// DatasourcesDefaults - default settings for all Datasources.
//easyjson:skip
type DatasourcesDefaults struct {
	RemoteCapTimestamp bool   `json:"kismet.datasourcetracker.default.remote_cap_timestamp"`
	RetryOnError       bool   `json:"kismet.datasourcetracker.default.retry_on_error"`
	RandomOrder        bool   `json:"kismet.datasourcetracker.default.random_order"`
	Split              bool   `json:"kismet.datasourcetracker.default.split"`
	Hop                bool   `json:"kismet.datasourcetracker.default.hop"`
	RemoteCapPort      int64  `json:"kismet.datasourcetracker.default.remote_cap_port"`
	HopRate            int64  `json:"kismet.datasourcetracker.default.hop_rate"`
	RemoteCapListen    string `json:"kismet.datasourcetracker.default.remote_cap_listen"`
}

var _ json.Unmarshaler = (*DatasourcesDefaults)(nil)

// datasourcesDefaultsRaw - raw DatasourcesDefaults.
//easyjson:skip
type datasourcesDefaultsRaw struct {
	DatasourcetrackerDefaultRemoteCapTimestamp int64  `json:"kismet.datasourcetracker.default.remote_cap_timestamp"`
	DatasourcetrackerDefaultRetryOnError       int64  `json:"kismet.datasourcetracker.default.retry_on_error"`
	DatasourcetrackerDefaultRandomOrder        int64  `json:"kismet.datasourcetracker.default.random_order"`
	DatasourcetrackerDefaultSplit              int64  `json:"kismet.datasourcetracker.default.split"`
	DatasourcetrackerDefaultHop                int64  `json:"kismet.datasourcetracker.default.hop"`
	DatasourcetrackerDefaultRemoteCapPort      int64  `json:"kismet.datasourcetracker.default.remote_cap_port"`
	DatasourcetrackerDefaultHopRate            int64  `json:"kismet.datasourcetracker.default.hop_rate"`
	DatasourcetrackerDefaultRemoteCapListen    string `json:"kismet.datasourcetracker.default.remote_cap_listen"`
}

func (kdd *DatasourcesDefaults) UnmarshalJSON(raw []byte) error {
	var rawParsed datasourcesDefaultsRaw
	if err := json.Unmarshal(raw, &rawParsed); err != nil {
		return err
	}

	kdd.RemoteCapTimestamp = rawParsed.DatasourcetrackerDefaultRemoteCapTimestamp == 1
	kdd.RetryOnError = rawParsed.DatasourcetrackerDefaultRetryOnError == 1
	kdd.RandomOrder = rawParsed.DatasourcetrackerDefaultRandomOrder == 1
	kdd.Split = rawParsed.DatasourcetrackerDefaultSplit == 1
	kdd.Hop = rawParsed.DatasourcetrackerDefaultHop == 1

	kdd.RemoteCapPort = rawParsed.DatasourcetrackerDefaultRemoteCapPort
	kdd.HopRate = rawParsed.DatasourcetrackerDefaultHopRate
	kdd.RemoteCapListen = rawParsed.DatasourcetrackerDefaultRemoteCapListen

	return nil
}

// Datasource - one Datasource configuration information.
//easyjson:skip
type Datasource struct { //nolint:dupl // Duplicate with a similar type in UnmarshalJSON.
	TypeDriver             DatasourceTypeDriver `json:"kismet.datasource.type_driver"`
	LinktypeOverride       bool                 `json:"kismet.datasource.linktype_override"`
	InfoAmpGain            int64                `json:"kismet.datasource.info.amp_gain"`
	InfoAmpType            string               `json:"kismet.datasource.info.amp_type"`
	InfoAntennaBeamwidth   int64                `json:"kismet.datasource.info.antenna_beamwidth"`
	InfoAntennaOrientation int64                `json:"kismet.datasource.info.antenna_orientation"`
	InfoAntennaGain        int64                `json:"kismet.datasource.info.antenna_gain"`
	InfoAntennaType        string               `json:"kismet.datasource.info.antenna_type"`
	TotalRetryAttempts     int64                `json:"kismet.datasource.total_retry_attempts"`
	RetryAttempts          int64                `json:"kismet.datasource.retry_attempts"`
	Retry                  bool                 `json:"kismet.datasource.retry"`
	PacketsDatasizeRrd     *CommonRrd           `json:"kismet.datasource.packets_datasize_rrd,omitempty"`
	PacketsRrd             CommonRrd            `json:"kismet.datasource.packets_rrd"`
	CaptureInterface       string               `json:"kismet.datasource.capture_interface"`
	Interface              string               `json:"kismet.datasource.interface"`
	Definition             string               `json:"kismet.datasource.definition"`
	UUID                   string               `json:"kismet.datasource.uuid"`
	Name                   string               `json:"kismet.datasource.name"`
	Passive                bool                 `json:"kismet.datasource.passive"`
	Remote                 bool                 `json:"kismet.datasource.remote"`
	Running                bool                 `json:"kismet.datasource.running"`
	IPCPID                 int64                `json:"kismet.datasource.ipc_pid"`
	IPCBinary              string               `json:"kismet.datasource.ipc_binary"`
	Paused                 bool                 `json:"kismet.datasource.paused"`
	SourceKey              int64                `json:"kismet.datasource.source_key"`
	SourceNumber           int64                `json:"kismet.datasource.source_number"`
	NumErrorPackets        int64                `json:"kismet.datasource.num_error_packets"`
	Hardware               string               `json:"kismet.datasource.hardware"`
	Dlt                    int64                `json:"kismet.datasource.dlt"`
	Warning                string               `json:"kismet.datasource.warning"`
	Channels               []string             `json:"kismet.datasource.channels"`
	Hopping                bool                 `json:"kismet.datasource.hopping"`
	Channel                string               `json:"kismet.datasource.channel"`
	HopRate                int64                `json:"kismet.datasource.hop_rate"`
	HopChannels            []string             `json:"kismet.datasource.hop_channels"`
	HopSplit               int64                `json:"kismet.datasource.hop_split"`
	HopOffset              int64                `json:"kismet.datasource.hop_offset"`
	HopShuffle             bool                 `json:"kismet.datasource.hop_shuffle"`
	HopShuffleSkip         int64                `json:"kismet.datasource.hop_shuffle_skip"`
	Error                  int64                `json:"kismet.datasource.error"`
	ErrorReason            string               `json:"kismet.datasource.error_reason"`
	NumPackets             int64                `json:"kismet.datasource.num_packets"`
}

var _ json.Unmarshaler = (*Datasource)(nil)

func (kds *Datasource) UnmarshalJSON(raw []byte) error {
	var parsedRaw struct { //nolint:dupl // Duplicate with a Datasource type.
		DatasourceTypeDriver             DatasourceTypeDriver `json:"kismet.datasource.type_driver"`
		DatasourceLinktypeOverride       int64                `json:"kismet.datasource.linktype_override"`
		DatasourceInfoAmpGain            int64                `json:"kismet.datasource.info.amp_gain"`
		DatasourceInfoAmpType            string               `json:"kismet.datasource.info.amp_type"`
		DatasourceInfoAntennaBeamwidth   int64                `json:"kismet.datasource.info.antenna_beamwidth"`
		DatasourceInfoAntennaOrientation int64                `json:"kismet.datasource.info.antenna_orientation"`
		DatasourceInfoAntennaGain        int64                `json:"kismet.datasource.info.antenna_gain"`
		DatasourceInfoAntennaType        string               `json:"kismet.datasource.info.antenna_type"`
		DatasourceTotalRetryAttempts     int64                `json:"kismet.datasource.total_retry_attempts"`
		DatasourceRetryAttempts          int64                `json:"kismet.datasource.retry_attempts"`
		DatasourceRetry                  int64                `json:"kismet.datasource.retry"`
		DatasourcePacketsDatasizeRrd     *CommonRrd           `json:"kismet.datasource.packets_datasize_rrd,omitempty"`
		DatasourcePacketsRrd             CommonRrd            `json:"kismet.datasource.packets_rrd"`
		DatasourceCaptureInterface       string               `json:"kismet.datasource.capture_interface"`
		DatasourceInterface              string               `json:"kismet.datasource.interface"`
		DatasourceDefinition             string               `json:"kismet.datasource.definition"`
		DatasourceUUID                   string               `json:"kismet.datasource.uuid"`
		DatasourceName                   string               `json:"kismet.datasource.name"`
		DatasourcePassive                int64                `json:"kismet.datasource.passive"`
		DatasourceRemote                 int64                `json:"kismet.datasource.remote"`
		DatasourceRunning                int64                `json:"kismet.datasource.running"`
		DatasourceIPCPID                 int64                `json:"kismet.datasource.ipc_pid"`
		DatasourceIPCBinary              string               `json:"kismet.datasource.ipc_binary"`
		DatasourcePaused                 int64                `json:"kismet.datasource.paused"`
		DatasourceSourceKey              int64                `json:"kismet.datasource.source_key"`
		DatasourceSourceNumber           int64                `json:"kismet.datasource.source_number"`
		DatasourceNumErrorPackets        int64                `json:"kismet.datasource.num_error_packets"`
		DatasourceHardware               string               `json:"kismet.datasource.hardware"`
		DatasourceDlt                    int64                `json:"kismet.datasource.dlt"`
		DatasourceWarning                string               `json:"kismet.datasource.warning"`
		DatasourceChannels               []string             `json:"kismet.datasource.channels"`
		DatasourceHopping                int64                `json:"kismet.datasource.hopping"`
		DatasourceChannel                string               `json:"kismet.datasource.channel"`
		DatasourceHopRate                int64                `json:"kismet.datasource.hop_rate"`
		DatasourceHopChannels            []string             `json:"kismet.datasource.hop_channels"`
		DatasourceHopSplit               int64                `json:"kismet.datasource.hop_split"`
		DatasourceHopOffset              int64                `json:"kismet.datasource.hop_offset"`
		DatasourceHopShuffle             int64                `json:"kismet.datasource.hop_shuffle"`
		DatasourceHopShuffleSkip         int64                `json:"kismet.datasource.hop_shuffle_skip"`
		DatasourceError                  int64                `json:"kismet.datasource.error"`
		DatasourceErrorReason            string               `json:"kismet.datasource.error_reason"`
		DatasourceNumPackets             int64                `json:"kismet.datasource.num_packets"`
	}

	if err := json.Unmarshal(raw, &parsedRaw); err != nil {
		return err
	}

	kds.Paused = parsedRaw.DatasourcePaused == 1
	kds.Passive = parsedRaw.DatasourcePassive == 1
	kds.Remote = parsedRaw.DatasourceRemote == 1
	kds.Running = parsedRaw.DatasourceRunning == 1
	kds.Hopping = parsedRaw.DatasourceHopping == 1
	kds.HopShuffle = parsedRaw.DatasourceHopShuffle == 1
	kds.LinktypeOverride = parsedRaw.DatasourceLinktypeOverride == 1
	kds.Retry = parsedRaw.DatasourceRetry == 1

	kds.TypeDriver = parsedRaw.DatasourceTypeDriver
	kds.InfoAmpGain = parsedRaw.DatasourceInfoAmpGain
	kds.InfoAmpType = parsedRaw.DatasourceInfoAmpType
	kds.InfoAntennaBeamwidth = parsedRaw.DatasourceInfoAntennaBeamwidth
	kds.InfoAntennaOrientation = parsedRaw.DatasourceInfoAntennaOrientation
	kds.InfoAntennaGain = parsedRaw.DatasourceInfoAntennaGain
	kds.InfoAntennaType = parsedRaw.DatasourceInfoAntennaType
	kds.TotalRetryAttempts = parsedRaw.DatasourceTotalRetryAttempts
	kds.RetryAttempts = parsedRaw.DatasourceRetryAttempts
	kds.PacketsDatasizeRrd = parsedRaw.DatasourcePacketsDatasizeRrd
	kds.PacketsRrd = parsedRaw.DatasourcePacketsRrd
	kds.CaptureInterface = parsedRaw.DatasourceCaptureInterface
	kds.Interface = parsedRaw.DatasourceInterface
	kds.Definition = parsedRaw.DatasourceDefinition
	kds.UUID = parsedRaw.DatasourceUUID
	kds.Name = parsedRaw.DatasourceName
	kds.IPCPID = parsedRaw.DatasourceIPCPID
	kds.IPCBinary = parsedRaw.DatasourceIPCBinary
	kds.SourceKey = parsedRaw.DatasourceSourceKey
	kds.SourceNumber = parsedRaw.DatasourceSourceNumber
	kds.NumErrorPackets = parsedRaw.DatasourceNumErrorPackets
	kds.Hardware = parsedRaw.DatasourceHardware
	kds.Dlt = parsedRaw.DatasourceDlt
	kds.Warning = parsedRaw.DatasourceWarning
	kds.Channels = parsedRaw.DatasourceChannels
	kds.Channel = parsedRaw.DatasourceChannel
	kds.HopRate = parsedRaw.DatasourceHopRate
	kds.HopChannels = parsedRaw.DatasourceHopChannels
	kds.HopSplit = parsedRaw.DatasourceHopSplit
	kds.HopOffset = parsedRaw.DatasourceHopOffset
	kds.HopShuffleSkip = parsedRaw.DatasourceHopShuffleSkip
	kds.Error = parsedRaw.DatasourceError
	kds.ErrorReason = parsedRaw.DatasourceErrorReason
	kds.NumPackets = parsedRaw.DatasourceNumPackets

	return nil
}

// DatasourceInterface - a Datasource interface information.
//easyjson:json
type DatasourceInterface struct {
	TypeDriver             DatasourceTypeDriver `json:"kismet.datasource.type_driver"`
	ProbedHardware         string               `json:"kismet.datasource.probed.hardware"`
	ProbedInUseUUID        string               `json:"kismet.datasource.probed.in_use_uuid"`
	ProbedOptionsVec       []interface{}        `json:"kismet.datasource.probed.options_vec"`
	ProbedCaptureInterface string               `json:"kismet.datasource.probed.capture_interface"`
	ProbedInterface        string               `json:"kismet.datasource.probed.interface"`
}

// GetDatasourcesSupportedTypesContext - returns supported Datasources.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#supported-datasource-types.
func (cl *Client) GetDatasourcesSupportedTypesContext(ctx context.Context) ([]DatasourceTypeDriver, error) {
	var supportedTypes []DatasourceTypeDriver

	if err := cl.getRequestContext(ctx, datasourcesTypesAllURL, nil, &supportedTypes); err != nil {
		return nil, err
	}

	return supportedTypes, nil
}

// GetDatasourcesSupportedTypes - returns supported Datasources.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#supported-datasource-types.
func (cl *Client) GetDatasourcesSupportedTypes() ([]DatasourceTypeDriver, error) {
	return cl.GetDatasourcesSupportedTypesContext(context.Background())
}

// GetDatasourcesDefaultsContext - returns Datasource default settings.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#defaults.
func (cl *Client) GetDatasourcesDefaultsContext(ctx context.Context) (*DatasourcesDefaults, error) {
	var defaults DatasourcesDefaults

	if err := cl.getRequestContext(ctx, datasourcesDefaultsURL, nil, &defaults); err != nil {
		return nil, err
	}

	return &defaults, nil
}

// GetDatasourcesDefaults - returns Datasource default settings.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#defaults.
func (cl *Client) GetDatasourcesDefaults() (*DatasourcesDefaults, error) {
	return cl.GetDatasourcesDefaultsContext(context.Background())
}

// GetDatasourcesAllContext - lists all Datasources.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#listing-datasources.
func (cl *Client) GetDatasourcesAllContext(ctx context.Context) ([]Datasource, error) {
	var datasources []Datasource

	if err := cl.getRequestContext(ctx, datasourcesAllURL, nil, &datasources); err != nil {
		return nil, err
	}

	return datasources, nil
}

// GetDatasourcesAll - lists all Datasources.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#listing-datasources.
func (cl *Client) GetDatasourcesAll() ([]Datasource, error) {
	return cl.GetDatasourcesAllContext(context.Background())
}

// GetDatasourceContext - returns one Datasource information.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#querying-specific-datasources.
func (cl *Client) GetDatasourceContext(ctx context.Context, datasourceUUID string) (*Datasource, error) {
	var ds Datasource

	if err := cl.getRequestContext(
		ctx,
		fmt.Sprintf(datasourceURL, url.PathEscape(datasourceUUID)),
		nil,
		&ds,
	); err != nil {
		return nil, err
	}

	return &ds, nil
}

// GetDatasource - returns one Datasource information.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#querying-specific-datasources.
func (cl *Client) GetDatasource(datasourceUUID string) (*Datasource, error) {
	return cl.GetDatasourceContext(context.Background(), datasourceUUID)
}

// GetDatasourcesInterfacesContext - lists all Interfaces which can potentially be used as Datasources.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#listing-interfaces.
func (cl *Client) GetDatasourcesInterfacesContext(ctx context.Context) ([]DatasourceInterface, error) {
	var interfaces []DatasourceInterface

	if err := cl.getRequestContext(ctx, datasourcesInterfacesAllURL, nil, &interfaces); err != nil {
		return nil, err
	}

	return interfaces, nil
}

// GetDatasourcesInterfaces - lists all Interfaces which can potentially be used as Datasources.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#listing-interfaces.
func (cl *Client) GetDatasourcesInterfaces() ([]DatasourceInterface, error) {
	return cl.GetDatasourcesInterfacesContext(context.Background())
}

// AddDatasourceContext - creates a new Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#adding-sources.
func (cl *Client) AddDatasourceContext(ctx context.Context, definition string) error {
	return cl.postCommandContext(
		ctx,
		datasourceAddCmdURL,
		commandDictionary{
			"definition": definition,
		})
}

// AddDatasource - creates a new Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#adding-sources.
func (cl *Client) AddDatasource(definition string) error {
	return cl.AddDatasourceContext(context.Background(), definition)
}

// SetDatasourceFixedChannelContext - sets one static Channel and stops channel hopping.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#setting-channels.
func (cl *Client) SetDatasourceFixedChannelContext(ctx context.Context, datasourceUUID, channel string) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(datasourceSetChannelCmdURL, url.PathEscape(datasourceUUID)),
		commandDictionary{
			"channel": channel,
		},
	)
}

// SetDatasourceFixedChannel - sets one static Channel and stops channel hopping.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#setting-channels.
func (cl *Client) SetDatasourceFixedChannel(datasourceUUID, channel string) error {
	return cl.SetDatasourceFixedChannelContext(context.Background(), datasourceUUID, channel)
}

// SetDatasourceChannelHoppingContext - sets channels to hop over for a Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#setting-channels.
func (cl *Client) SetDatasourceChannelHoppingContext(ctx context.Context,
	datasourceUUID string, channels []string, rate *int, shuffle *bool) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(datasourceSetChannelCmdURL, url.PathEscape(datasourceUUID)),
		commandDictionary{
			"channels": channels,
			"rate":     rate,
			"shuffle":  shuffle,
		},
	)
}

// SetDatasourceChannelHopping - sets channels to hop over for a Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#setting-channels.
func (cl *Client) SetDatasourceChannelHopping(ctx context.Context,
	datasourceUUID string, channels []string, rate *int, shuffle *bool) error {
	return cl.SetDatasourceChannelHoppingContext(context.Background(), datasourceUUID, channels, rate, shuffle)
}

// SetDatasourceChannelHoppingEnableContext - enables channel hopping for a datasource over all available channels.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#set-channel-hopping.
func (cl *Client) SetDatasourceChannelHoppingEnableContext(ctx context.Context, datasourceUUID string) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(datasourceSetChannelHoppingCmdURL, url.PathEscape(datasourceUUID)),
		nil,
	)
}

// SetDatasourceChannelHoppingEnable - enables channel hopping for a datasource over all available channels.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#set-channel-hopping.
func (cl *Client) SetDatasourceChannelHoppingEnable(datasourceUUID string) error {
	return cl.SetDatasourceChannelHoppingEnableContext(context.Background(), datasourceUUID)
}

// OpenDatasourceContext - opens a defined Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#opening-closed-sources.
func (cl *Client) OpenDatasourceContext(ctx context.Context, datasourceUUID string) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(datasourceOpenSourceCmdURL, url.PathEscape(datasourceUUID)),
		nil,
	)
}

// OpenDatasource - opens a defined Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#opening-closed-sources.
func (cl *Client) OpenDatasource(datasourceUUID string) error {
	return cl.OpenDatasourceContext(context.Background(), datasourceUUID)
}

// CloseDatasourceContext - closes one Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#closing-sources.
func (cl *Client) CloseDatasourceContext(ctx context.Context, datasourceUUID string) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(datasourceCloseSourceCmdURL, url.PathEscape(datasourceUUID)),
		nil,
	)
}

// CloseDatasource - closes one Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#closing-sources.
func (cl *Client) CloseDatasource(datasourceUUID string) error {
	return cl.CloseDatasourceContext(context.Background(), datasourceUUID)
}

// PauseDatasourceContext - pauses one Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#pausing-sources.
func (cl *Client) PauseDatasourceContext(ctx context.Context, datasourceUUID string) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(datasourcePauseSourceCmdURL, url.PathEscape(datasourceUUID)),
		nil,
	)
}

// PauseDatasource - pauses one Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#pausing-sources.
func (cl *Client) PauseDatasource(datasourceUUID string) error {
	return cl.PauseDatasourceContext(context.Background(), datasourceUUID)
}

// ResumeDatasourceContext - resumes one Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#resuming-sources.
func (cl *Client) ResumeDatasourceContext(ctx context.Context, datasourceUUID string) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(datasourceResumeSourceCmdURL, url.PathEscape(datasourceUUID)),
		nil,
	)
}

// ResumeDatasource - resumes one Datasource.
// https://www.kismetwireless.net/docs/devel/webui_rest/datasources/#resuming-sources.
func (cl *Client) ResumeDatasource(datasourceUUID string) error {
	return cl.ResumeDatasourceContext(context.Background(), datasourceUUID)
}
