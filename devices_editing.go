package kismet

import (
	"context"
	"fmt"
	"net/url"
)

const (
	devicesSetNameCMDURL = "/devices/by-key/%s/set_name.cmd"
	devicesSetTagCMDURL  = "/devices/by-key/%s/set_tag.cmd"
)

// SetDeviceNameContext - renames a Device.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#editing---device-names.
func (cl *Client) SetDeviceNameContext(ctx context.Context, deviceKey, newName string) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(devicesSetNameCMDURL, url.PathEscape(deviceKey)),
		commandDictionary{"username": newName},
	)
}

// SetDeviceName - renames a Device.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#editing---device-names.
func (cl *Client) SetDeviceName(deviceKey, newName string) error {
	return cl.SetDeviceNameContext(context.Background(), deviceKey, newName)
}

// SetDeviceTagContext - adds a Tag to a Device.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#editing---device-tags.
func (cl *Client) SetDeviceTagContext(ctx context.Context, deviceKey, tagName, tagValue string) error {
	return cl.postCommandContext(
		ctx,
		fmt.Sprintf(devicesSetTagCMDURL, url.PathEscape(deviceKey)),
		commandDictionary{
			"tagname":  tagName,
			"tagvalue": tagValue,
		},
	)
}

// SetDeviceTag - adds a Tag to a Device.
// https://www.kismetwireless.net/docs/devel/webui_rest/devices/#editing---device-tags.
func (cl *Client) SetDeviceTag(deviceKey, tagName, tagValue string) error {
	return cl.SetDeviceTagContext(context.Background(), deviceKey, tagName, tagValue)
}
