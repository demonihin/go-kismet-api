package kismet

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const RequestTimeout = 10 * time.Second

var (
	ErrRequestFailed = errors.New("http request to  API failed")
)

// Client - API Client.
//easyjson:skip
type Client struct {
	httpClient http.Client
	apiToken   string
	serverURL  string
}

func New(serverURL *url.URL, apiToken string) (*Client, error) {
	var cl = &Client{
		httpClient: http.Client{
			Timeout: RequestTimeout,
		},
		apiToken:  apiToken,
		serverURL: strings.TrimRight(serverURL.String(), "/"),
	}

	return cl, nil
}

func (cl *Client) doRequestContext(
	ctx context.Context,
	method string, urlSuffix string,
	options, result interface{}) error {
	// Command.
	reqBody, err := json.Marshal(options)
	if err != nil {
		return err
	}

	var data = &url.Values{}
	data.Set("json", string(reqBody))
	var encodedData = data.Encode()

	// Request.
	req, err := http.NewRequestWithContext(ctx, method,
		cl.serverURL+urlSuffix, strings.NewReader(encodedData))
	if err != nil {
		return err
	}

	// Authentication.
	req.AddCookie(&http.Cookie{
		Name:  "KISMET",
		Value: cl.apiToken,
	})

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(encodedData)))

	// Send.
	resp, err := cl.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Check response code.
	if !(resp.StatusCode >= 200 && resp.StatusCode < 300) {
		bts, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		return fmt.Errorf("%w: status code is %d, server message: %s", ErrRequestFailed, resp.StatusCode, string(bts))
	}

	// Get response.
	if result == nil {
		return nil
	}

	var decoder = json.NewDecoder(resp.Body)

	decoder.DisallowUnknownFields()

	return decoder.Decode(result)
}

func (cl *Client) getRequestContext(ctx context.Context,
	urlSuffix string,
	options *QueryOptions, result interface{}) error { //nolint:unparam // May be good to remove it in a future.
	return cl.doRequestContext(ctx, http.MethodGet, urlSuffix, options, result)
}

func (cl *Client) postRequestContext(ctx context.Context,
	urlSuffix string,
	options *QueryOptions, result interface{}) error {
	return cl.doRequestContext(ctx, http.MethodPost, urlSuffix, options, result)
}

func (cl *Client) postCommandContext(ctx context.Context, urlSuffix string, command commandDictionary) error {
	return cl.doRequestContext(ctx, http.MethodPost, urlSuffix, command, nil)
}

func urlMustParse(s string) *url.URL {
	u, err := url.Parse(s)
	if err != nil {
		panic(fmt.Errorf("can not parse %q to url.URL: %w", s, err))
	}

	return u
}
