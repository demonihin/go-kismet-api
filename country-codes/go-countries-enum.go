package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

const (
	CountriesFile            = "802.11-country-codes.csv"
	CountriesDataType        = "Dot11AdvertisedSSIDDot11DCountryEnum"
	CountriesConstNamePrefix = "Dot11Country"
)

func main() {
	file, err := os.Open(CountriesFile)
	if err != nil {
		log.Fatalln(err)
	}

	reader := csv.NewReader(file)

	records, err := reader.ReadAll()
	if err != nil {
		log.Fatalln(err)
	}

	sort.Slice(records, func(i, j int) bool {
		return records[i][1] < records[j][1]
	})

	for _, rec := range records {
		var (
			code        = rec[0]
			countryName = rec[1]
		)

		countryName = strings.ReplaceAll(countryName, " ", "")

		fmt.Printf("%s%s %s = %q\n", CountriesConstNamePrefix, countryName, CountriesDataType, code)
	}
}
