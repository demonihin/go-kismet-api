package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestSystemStatusSuite struct {
	suite.Suite
}

func (t *TestSystemStatusSuite) TestGetSystemStatusContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	status, err := cl.GetSystemStatusContext(context.Background(), nil)
	t.NoError(err)
	t.NotNil(status)

	out, _ := json.MarshalIndent(status, "", "  ")
	fmt.Println(string(out))
}

func (t *TestSystemStatusSuite) TestGetSystemTimestampContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	ts, err := cl.GetSystemTimestampContext(context.Background())
	t.NoError(err)
	t.NotNil(ts)

	out, _ := json.MarshalIndent(ts, "", "  ")
	fmt.Println(string(out))
}

func (t *TestSystemStatusSuite) TestGetSystemPacketStatsContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	ts, err := cl.GetSystemPacketStatsContext(context.Background(), nil)
	t.NoError(err)
	t.NotNil(ts)

	out, _ := json.MarshalIndent(ts, "", "  ")
	fmt.Println(string(out))
}

func TestRunSystemStatusSuite(t *testing.T) {
	suite.Run(t, &TestSystemStatusSuite{})
}
