package kismet

import "context"

// Channels - channel info.
//easyjson:json
type Channels struct {
	FrequencyMap map[string]ChanneltrackerFrequencyMap `json:"kismet.channeltracker.frequency_map"`
	ChannelMap   ChanneltrackerChannelMap              `json:"kismet.channeltracker.channel_map"`
}

// ChanneltrackerChannelMap - channels map, empty for now.
//easyjson:json
type ChanneltrackerChannelMap struct {
}

// ChanneltrackerFrequencyMap - a channel's frequency statistics.
//easyjson:json
type ChanneltrackerFrequencyMap struct {
	Signal CommonSignal `json:"kismet.channelrec.signal"`
	// DeviceRrd - seen devices count.
	DeviceRrd CommonRrd `json:"kismet.channelrec.device_rrd"`
	// DataRrd - received bytes count.
	DataRrd CommonRrd `json:"kismet.channelrec.data_rrd"`
	// PacketsRrd - received packets count.
	PacketsRrd CommonRrd `json:"kismet.channelrec.packets_rrd"`
	Frequency  int64     `json:"kismet.channelrec.frequency"`
	Channel    string    `json:"kismet.channelrec.channel"`
}

const (
	channelsAllURL = "/channels/channels.json"
)

// GetChannelsContext gets a dictionary containing channel usage, device counts, and coverage data.
// https://www.kismetwireless.net/docs/devel/webui_rest/channels/#channels.
func (cl *Client) GetChannelsContext(ctx context.Context) (*Channels, error) {
	var chanReport Channels

	if err := cl.getRequestContext(ctx, channelsAllURL, nil, &chanReport); err != nil {
		return nil, err
	}

	return &chanReport, nil
}

// GetChannels gets a dictionary containing channel usage, device counts, and coverage data.
// https://www.kismetwireless.net/docs/devel/webui_rest/channels/#channels.
func (cl *Client) GetChannels() (*Channels, error) {
	return cl.GetChannelsContext(context.Background())
}
