package kismet

import (
	"log"
	"os"
)

var (
	serverURL = urlMustParse(getEnvPanic("KISMET_SERVER_URL"))
	apiToken  = getEnvPanic("KISMET_API_TOKEN")
)

func getEnvPanic(name string) string {
	env, ok := os.LookupEnv(name)
	if !ok {
		log.Fatalf("Can not get an Environment Variable %q, empty value", name)
	}

	return env
}
