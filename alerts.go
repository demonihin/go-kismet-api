package kismet

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
)

const (
	alertsDefinitionsURL  = "/alerts/definitions.json"
	alertsAllURL          = "/alerts/all_alerts.json"
	alertsLastSeenURL     = "/alerts/last-time/%d.%d/alerts.json"
	alertsAlertDetailsURL = "/alerts/by-id/%s/alert.json"
)

// AlertSeverity Alerts severity.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#alert-severities
type AlertSeverity int64

// 0 	INFO 	Informational alerts, such as datasource errors,  state changes, etc
// 5 	LOW 	Low-risk events such as probe fingerprints
// 10 	MEDIUM 	Medium-risk events such as denial of service attempts
// 15 	HIGH 	High-risk events such as fingerprinted watched devices, denial of service attacks, and similar
// 20 	CRITICAL 	Critical errors such as fingerprinted known exploits

const (
	AlertSeverityInfo     AlertSeverity = 0
	AlertSeverityLow      AlertSeverity = 5
	AlertSeverityMedium   AlertSeverity = 10
	AlertSeverityHigh     AlertSeverity = 15
	AlertSeverityCritical AlertSeverity = 20
)

func (as AlertSeverity) String() string {
	return alertSeverityMap[as]
}

// alertSeverityMap maps alert severity numbers to names.
var alertSeverityMap = map[AlertSeverity]string{ //nolint:gochecknoglobals
	AlertSeverityInfo:     "INFO",
	AlertSeverityLow:      "LOW",
	AlertSeverityMedium:   "MEDIUM",
	AlertSeverityHigh:     "HIGH",
	AlertSeverityCritical: "CRITICAL",
}

// AlertDefinitionClass type of Alert.
type AlertDefinitionClass string

const (
	// AlertTypeDenial - Possible denial of service attack.
	AlertTypeDenial AlertDefinitionClass = "DENIAL"
	// AlertTypeExploit - Known fingerprinted exploit attempt against a vulnerability.
	AlertTypeExploit AlertDefinitionClass = "EXPLOIT"
	// AlertTypeOther - General category for alerts which don’t fit in any existing bucket.
	AlertTypeOther AlertDefinitionClass = "OTHER"
	// AlertTypeProbe - Probe by known tools.
	AlertTypeProbe AlertDefinitionClass = "PROBE"
	// AlertTypeSpoof - Attempt to spoof an existing device.
	AlertTypeSpoof AlertDefinitionClass = "SPOOF"
	// AlertTypeSystem - System events, such as log changes, datasource errors, etc.
	AlertTypeSystem AlertDefinitionClass = "SYSTEM"
)

// AlertDefinition Alert type definition.
//easyjson:json
type AlertDefinition struct {
	TimeLast    TimeUSec             `json:"kismet.alert.definition.time_last"`
	TotalSent   int64                `json:"kismet.alert.definition.total_sent"`
	BurstSent   int64                `json:"kismet.alert.definition.burst_sent"`
	LimitBurst  int64                `json:"kismet.alert.definition.limit_burst"`
	BurstUnit   int64                `json:"kismet.alert.definition.burst_unit"`
	LimitRate   int64                `json:"kismet.alert.definition.limit_rate"`
	LimitUnit   int64                `json:"kismet.alert.definition.limit_unit"`
	Phyid       int64                `json:"kismet.alert.definition.phyid"`
	Description string               `json:"kismet.alert.definition.description"`
	Severity    AlertSeverity        `json:"kismet.alert.definition.severity"`
	Class       AlertDefinitionClass `json:"kismet.alert.definition.class"`
	Header      string               `json:"kismet.alert.definition.header"`
}

// Alert - Kismet Alert.
//easyjson:json
type Alert struct {
	Location       AlertLocation        `json:"kismet.alert.location"`
	Text           string               `json:"kismet.alert.text"`
	DeviceKey      string               `json:"kismet.alert.device_key"`
	Header         string               `json:"kismet.alert.header"`
	Class          AlertDefinitionClass `json:"kismet.alert.class"`
	Hash           int64                `json:"kismet.alert.hash"`
	Severity       AlertSeverity        `json:"kismet.alert.severity"`
	PhyID          int64                `json:"kismet.alert.phy_id"`
	Timestamp      TimeUSec             `json:"kismet.alert.timestamp"`
	TransmitterMAC string               `json:"kismet.alert.transmitter_mac"`
	SourceMAC      string               `json:"kismet.alert.source_mac"`
	DestMAC        string               `json:"kismet.alert.dest_mac"`
	OtherMAC       string               `json:"kismet.alert.other_mac"`
	Channel        string               `json:"kismet.alert.channel"`
	Frequency      int64                `json:"kismet.alert.frequency"`
}

// AlertLocation - an alert's geographic location.
//easyjson:skip
type AlertLocation struct {
	Time     TimeUSec `json:"kismet.common.location.time"`
	Fix      int64    `json:"kismet.common.location.fix"`
	Alt      int64    `json:"kismet.common.location.alt"`
	Geopoint []int64  `json:"kismet.common.location.geopoint"`
}

// alertLocationRaw - raw AlertLocation.
//easyjson:json
type alertLocationRaw struct {
	CommonLocationTimeUsec int64   `json:"kismet.common.location.time_usec"`
	CommonLocationTimeSEC  int64   `json:"kismet.common.location.time_sec"`
	CommonLocationFix      int64   `json:"kismet.common.location.fix"`
	CommonLocationAlt      int64   `json:"kismet.common.location.alt"`
	CommonLocationGeopoint []int64 `json:"kismet.common.location.geopoint"`
}

var _ json.Unmarshaler = (*AlertLocation)(nil)

func (al *AlertLocation) UnmarshalJSON(raw []byte) error {
	var parsedRaw alertLocationRaw

	if err := json.Unmarshal(raw, &parsedRaw); err != nil {
		return err
	}

	al.Alt = parsedRaw.CommonLocationAlt
	al.Fix = parsedRaw.CommonLocationFix
	al.Geopoint = parsedRaw.CommonLocationGeopoint

	al.Time.Microseconds = int32(parsedRaw.CommonLocationTimeUsec)
	al.Time.Seconds = parsedRaw.CommonLocationTimeSEC

	return nil
}

// GetAlertsDefinitionsContext - returns full Alerts configuration and currently supported alert types.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#alert-configuration.
func (cl *Client) GetAlertsDefinitionsContext(ctx context.Context) ([]AlertDefinition, error) {
	var defs []AlertDefinition

	if err := cl.getRequestContext(ctx, alertsDefinitionsURL, nil, &defs); err != nil {
		return nil, err
	}

	return defs, nil
}

// GetAlertsDefinitions - returns full Alerts configuration and currently supported alert types.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#alert-configuration.
func (cl *Client) GetAlertsDefinitions() ([]AlertDefinition, error) {
	return cl.GetAlertsDefinitionsContext(context.Background())
}

// GetAlertsAllContext - returns last Alerts.
// Kismet retains the past N alerts, as defined by alertbacklog in kismet_memory.conf.
// By default, Kismet retains 50 alert records.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#all-alerts.
func (cl *Client) GetAlertsAllContext(ctx context.Context) ([]Alert, error) {
	var alerts []Alert

	if err := cl.getRequestContext(ctx, alertsAllURL, nil, &alerts); err != nil {
		return nil, err
	}

	return alerts, nil
}

// GetAlertsAll - returns last Alerts.
// Kismet retains the past N alerts, as defined by alertbacklog in kismet_memory.conf.
// By default, Kismet retains 50 alert records.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#all-alerts.
func (cl *Client) GetAlertsAll() ([]Alert, error) {
	return cl.GetAlertsAllContext(context.Background())
}

// GetAlertsLastSeenContext - returns Alerts since timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#recent-alerts.
func (cl *Client) GetAlertsLastSeenContext(ctx context.Context, timestamp TimeUSec) ([]Alert, error) {
	var alerts []Alert

	if err := cl.getRequestContext(
		ctx,
		fmt.Sprintf(alertsLastSeenURL, timestamp.Seconds, timestamp.Microseconds),
		nil,
		&alerts); err != nil {
		return nil, err
	}

	return alerts, nil
}

// GetAlertsLastSeen - returns Alerts since timestamp.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#recent-alerts.
func (cl *Client) GetAlertsLastSeen(timestamp TimeUSec) ([]Alert, error) {
	return cl.GetAlertsLastSeenContext(context.Background(), timestamp)
}

// GetAlertDetailsContext - returns details of one Alert.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#alert-details.
func (cl *Client) GetAlertDetailsContext(ctx context.Context,
	alertHash string, fields FieldSimplification) (*Alert, error) {
	var alert Alert

	if err := cl.postRequestContext(
		ctx,
		fmt.Sprintf(alertsAlertDetailsURL, url.PathEscape(alertHash)),
		&QueryOptions{Fields: fields},
		&alert); err != nil {
		return nil, err
	}

	return &alert, nil
}

// GetAlertDetails - returns details of one Alert.
// https://www.kismetwireless.net/docs/devel/webui_rest/alerts/#alert-details.
func (cl *Client) GetAlertDetails(alertHash string, fields FieldSimplification) (*Alert, error) {
	return cl.GetAlertDetailsContext(context.Background(), alertHash, fields)
}
