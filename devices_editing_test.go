package kismet

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestDevicesEditingSuite struct {
	suite.Suite
}

func (t *TestDevicesEditingSuite) TestSetDeviceNameContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.SetDeviceNameContext(context.Background(), "4202770D00000000_21C2E45A8F48", "Renamed #Telia-9206A8")

	t.NoError(err)
}

func (t *TestDevicesEditingSuite) TestSetDeviceTagContext() {
	cl, err := New(serverURL, apiToken)
	t.NoError(err, "Can not create API client")

	err = cl.SetDeviceTagContext(context.Background(), "4202770D00000000_21C2E45A8F48", "new-tag-2", "new-tag-2 value")

	t.NoError(err)
}

func TestRunDevicesEditingSuite(t *testing.T) {
	suite.Run(t, &TestDevicesEditingSuite{})
}
